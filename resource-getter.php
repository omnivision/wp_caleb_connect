<?php
/*
* file: getter.php - pulls the resources from Caleb and saves them as posts.
* @author Chris Young <chris.young@om.org>
*/

require_once ABSPATH . 'wp-admin/includes/plugin.php';

class CalebResourceGetter
{

	var $base_url = OM_APP_RESOURCES_URL;
	var $single_baseurl = OM_APP_RESOURCE_URL;
	var $resources = array();
	var $posts = array();
	var $num_imports;
	var $num_updates;
	var $num_discarded;
	var $num_deleted;
	var $fetched_urls = array();
	var $options;
	var $caleb_wordpress_format;

	function __construct()
	{
		include('resource-options.php');

		// Get options and put the feed options into a nested array for ease of use.
		$this->options = wp_parse_args(get_option('caleb-connect-resource_options'));
		for ($i=0; $i<$this->options['number_of_feeds']; $i++) {
			foreach ($this->options as $k=>$v) {
				if (strpos($k, (String)$i)==(strlen($k)-1)) {
					$this->options['feeds'][$i][substr($k, 0, -2)] = $v;
				}
			}
		}

		$this->options = array_merge($this->options, wp_parse_args(get_option('caleb-connect-general_options')));

		foreach ($this->options['feeds'] as $feed_id=>$feed_options) {
			foreach ($feed_options as $option_name=>$option_value) {
				if ($result = @unserialize($option_value)) {
					$this->options['feeds'][$feed_id][$option_name] = $result;
				} else {
					//$this->options['feeds'][$feed_id][$option_name] = $option_value;
				}
			}
		}
	}

	/**
	 * changes category to the current values.
	 */
	function addWPMLcategory(&$post, $lang)
	{
		global $sitepress;

		// translation group id:
		$trid = $sitepress->get_element_trid($post['post_category'][0], 'tax_category');
		if (!$trid) {
			return $trid;
		}


		// get a list of all languages in this group:
		$translations = $sitepress->get_element_translations($trid, 'tax_category');
		if (isset($translations[$lang])) {
			// replace category:
			$post['post_category'] = array($translations[$lang]->element_id);
		}

	}


	function getStats()
	{
		return array(
			"num_imports"=>(int)$this->num_imports,
			"num_updates"=>(int)$this->num_updates,
			"num_discarded"=>(int)$this->num_discarded,
			"num_deleted"=>(int)$this->num_deleted,
			"urls"=>implode(',', $this->fetched_urls),
			);
	}

	/*
	* function getNow() - get immediately rather than via CRON job
	* returns integer - number of affected rows.
	*/
	function getNow()
	{
		$result = $this->get();

		if (is_wp_error($result)) {
			foreach ($result->errors as $error_group=>$errors) {
				foreach ($errors as $msg) {
					set_transient('caleb-connect-resource-error', array(time(), $error_group, $msg));
				}
			}
			return false;
		}
		return $result;
	}

	/*
	* function runCron() - run the CRON job
	*/
	function runCron()
	{
		$result = $this->get();

		if (is_wp_error($result)) {
			foreach ($result->errors as $error_group=>$errors) {
				foreach ($errors as $msg) {
					set_transient('caleb-connect-resource-error', array(time(), $error_group, $msg));
				}
			}
			return false;
		}
		return $result;
	}

	/*
	* function: get() - pull and save posts
	* returns an error if there was one
	*/
	function get()
	{
		$max_posts=50;
		$result = $this->pull();
		if (is_wp_error($result)) {
			return $result;
		}

		// Check to see if any posts exist that are not in our list
		$result = $this->checkPosts();
		if (is_wp_error($result)) {
			return $result;
		} else {
			$this->num_deleted = $result;
		}

		// Import the resources in reverse date order - don't just do the first feed first!
		$sort = array();
		foreach ($this->resources as $key => $row) {
			$sort[$key] = strtotime($row->pubDate);
		}

		array_multisort($sort, SORT_DESC, $this->resources);

		foreach ($this->resources as $item) {
			$om = $item->children('om', true);
			$resource['id'] = $om->id;
			$query = new WP_Query('suppress_filters=1&meta_key=_caleb_id&meta_value='.$om->id);
			if ($query->have_posts()) { // If this resource already exists
				$post = $query->posts[0];
				// Find out if it's modification date has changed, and if so, update this post.
				if ($om->modifiedDate != get_post_meta($query->posts[0]->ID, '_modified_date', true)) {
					if ($resource = $this->getResource($om->id)) {
						$resource['post_id'] = $post->ID;
						$resource['feed_id'] = (Int)$item['feed_id'];
						$resource['meta']['_modified_date'] = (String)$om->modifiedDate;
						$this->posts[] = $resource;
					}
				}
			} else { // Prepare a new post for insertion.
				if ($resource = $this->getResource($om->id)) {
					$resource['feed_id'] = (Int)$item['feed_id'];
					$resource['meta']['_modified_date'] = (String)$om->modifiedDate;
					$this->posts[] = $resource;
				}
			}
			// Without this check - maximum execution time could be exceeded.
			// On next run, the next $max_posts posts will be processed.
			if (count($this->posts) >= $max_posts) {
				break;
			}
		}
		$result = $this->save();
		return $result;
	}

	/*
	* function pullFromCaleb() - actually request the resources from caleb
	* returns: array of resources from caleb
	*/
	function pull()
	{

		for ($i=0;$i<$this->options['number_of_feeds'];$i++) {
			$feed_opt = $this->options['feeds'][$i];
			$feed_url = $this->base_url."?plain=1";
			if (count($feed_opt['resource_type'])) {
				foreach ($feed_opt['resource_type'] as $mediaTypeId) {
					$feed_url .= '&mediaTypeId='.$mediaTypeId;
				}
			}
			$from_date = $this->options['from_date_'.$i];
			if ($from_date == 0) {
				$from_date = time()-(60*60*24*365);
			}

			$feed_url .= "&fromYear=".date('Y', $from_date);
			$feed_url .= "&fromMonth=".date('m', $from_date);
			$feed_url .= "&fromDOM=".date('d', $from_date);
			$feed_url .= '&length=5000';

			if (isset($feed_opt['country']) && (count($feed_opt['country']))) {
				foreach ($feed_opt['country'] as $country) {
					$feed_url .= '&countryId='.$country;
				}
			}
			if (isset($feed_opt['category']) && (count($feed_opt['category']))) {
				foreach ($feed_opt['category'] as $category) {
					$feed_url .= '&categoryid='.$category;
				}
			}
			if ((isset($feed_opt['language'])) && ($feed_opt['language'])) {
				$feed_url .= '&text=LANG_'.$feed_opt['language'];
			}
			if ((isset($this->options["field_id"])) && ($this->options["field_id"])) {
				$feed_url .= '&field='.$this->options["field_id"];
			}
			if ((isset($this->options["pin"])) && ($this->options["pin"])) {
				$feed_url .= '&pin='.$this->options["pin"];
			}

			$url = html_entity_decode($feed_url);
			$this->fetched_urls[] = urlencode($feed_url);
			$response = wp_remote_get($url, array('timeout' => 15));

			if (!is_wp_error($response)) {
				$data = wp_remote_retrieve_body($response);

				$data = simplexml_load_string($data);

				foreach($data->channel->item as $resource) {
					$resource['feed_id'] = $i;
					$om = $resource->children('om', true);
					if ($this->options['discard_articles_with_no_image_'.$i]) {
						if (!$om->thumbnailUrl) {
							$this->num_discarded++;
							continue;
						}
					}
					$this->resources[] = $resource;
				}
			}
		}
		return true;
	}

	/*
	* function: getCalebResource(String $resource_id)
	* Get a single resource from caleb using the ID string
	* Returns an array representing the resource
	*/
	function getResource($resource_id)
	{
		$url = html_entity_decode($this->single_baseurl."?id=".$resource_id);
		$response = wp_remote_get($url, array('timeout' => 15));

		if (is_wp_error($response)) {
			return $response;
		}

		$data = wp_remote_retrieve_body($response);

		$data = simplexml_load_string($data);

		$item = $data->channel->item;

		// Test to see if an actual item is returned.
		$item = $data->channel->item;

		if (!@count($item)) {
			return false;
		}

		$om = $item->children('om', true);

		if (in_array($om->mediaTypeId, array(4, 5))) {
			$full = $om->full;
			$endp1 = strpos($full, '.', 90);
			$p1 = substr($full, 0, $endp1 + 1);
			$rest = substr($full, $endp1 + 1);
			$full = $p1.'<!--more-->'.$rest;
		}

		foreach ($om->categories as $category) {
			$categories[] = (String)$category;
		}

		$categories = implode(',', $categories);
		$resource = array(
            'mediaTypeId' => (int)$om->mediaTypeId,
            'title'       => (String)$om->title,
            'full'        => (String)$full,
            'pubDate'     => (String)$item->pubDate,
            'description' => (String)$item->description,
            'meta'        => array(
                'Region'               => $om->region,
                'Country'              => $om->country,
                '_author_name'         => $om->authorName,
                '_author_email'        => $om->authorEmail,
                '_owner_name'          => $om->ownerName,
                '_owner_email'         => $om->ownerEmail,
                '_copyright'           => $om->copyrightDescription,
                '_credit'              => $om->creditDescription,
                '_modification_rights' => $om->ModificationDescription,
                '_creation_date'       => $om->creationDate,
                '_modified_date'       => $om->modifiedDate,
                '_caleb_pubDate'       => $item->pubDate,
                '_caleb_id'            => $om->id,
                '_caleb_type'          => 'resource',
				),
			'description'=>(String)$om->description,
			'mediaUrl'=>(String)$om->mediaUrl,
			'image'=>($om->attachedPhotoId ? 'http://www.om.org/img/m'.$om->attachedPhotoId.'.jpg':null),
			'image_title'=>(String)$om->attachedPhotoTitle,
			'categories'=>$categories
			);
		return $resource;
	}

	/*
	* function saveCalebPosts(array $resources) - save new resources as posts in wordpress
	* returns integer - number of rows inserted
	*/
	function save()
	{
		require_once(ABSPATH . 'wp-admin/includes/media.php');
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		foreach ($this->posts as $resource) {

			$lang = $this->options['feeds'][$resource['feed_id']]['language'];
			$category = $this->options['feeds'][$resource['feed_id']]['wp_category'];

			if ((isset($resource['post_id'])) && ($resource['post_id'])) {
				$post = $resource;
				$post['ID'] = $resource['post_id'];
				$new = false;
			} else {
				$new = true;

				// if we're in WPML land, set the language & taxonomy correctly.

				$post = array(
                    'post_title'    => $resource['title'],
                    'post_name'     => sanitize_title($resource['title'].'-'.(String)$resource['meta']['_caleb_id']),
                    'post_category' => array($category),
                    'tags_input'    => $this->options['feeds'][$resource['feed_id']]['wp_tags']. ',' . $resource['categories'],
                    'post_status'   => $this->options['feeds'][$resource['feed_id']]['wp_status'],
                    'comment_status'=> $this->options['feeds'][$resource['feed_id']]['wp_comment_status'],
                    'post_date'     => date('Y-m-d H:i:s', strtotime($resource['pubDate'])),
					);

				if(function_exists('wpml_get_content_translation')) {
					$this->addWPMLcategory($post, $lang);
				}

				$post['ID'] = wp_insert_post($post, true);

				if (is_wp_error($post['ID'])) {
					return $post['ID'];
				}
			}

			if (function_exists('wpml_update_translatable_content')) {
				// and set the WPML language of the post to the correct language:
				$_POST['icl_post_language'] = $lang; // stupid hack! wpml_update... doesn't work without...
				wpml_update_translatable_content('post_post', $post['ID'], $lang);
			}

			$meta = "";
			foreach ($resource['meta'] as $key=>$val) {
				if ($val && substr($key, 0, 1)!='_') {
					$meta .= "<b>$key:</b> $val\n";
				}
			}
			$meta = substr($meta, 0, -1);

			if (array_key_exists('add_qtranslate_flags', $this->options) && ($this->options['add_qtranslate_flags'] == '1')) {
				$content_prefix = '<!--:' . $this->options['feeds'][$resource['feed_id']]['language'] . '-->';
			} else {
				$content_prefix = '';
			}

			$post = array(
				'ID'			=> $post['ID'],
				'post_content'  => $content_prefix
								   . str_replace(array(" \n", '<br />', '<p>', '</p>'), "\n",
												 html_entity_decode($resource['full'], ENT_QUOTES, 'UTF-8'))
												 . "<div class=\"om_resource_meta\">$meta</div>",
				'post_excerpt'  => $resource['description'],
				'post_title'	=> $resource['title'],
				'post_date'	 => date('Y-m-d H:i:s', strtotime($resource['pubDate'])),
				'edit_date'	 => true,
				);

			set_post_format($post['ID'], $this->caleb_wordpress_format[$resource['mediaTypeId']]);
			if ($new) {
				// only (try to) download images for new posts...

				if (in_array($resource['mediaTypeId'], array(0, 1))) {
					$img_tag = media_sideload_image($resource['mediaUrl'], $post['ID']);
					if (is_wp_error($img_tag)) {
						return $img_tag;
					}
					$post['post_content'] .= $img_tag;
				} elseif ($resource['mediaTypeId'] == 5) {
					// ????
				} elseif (false === $this->set_featured_image($resource['image'], $post['ID'], $resource['image_title'])) {
					if ($this->options['feeds'][$resource['feed_id']]['discard_articles_with_no_image']) {
						$delete = wp_delete_post($post['ID']);

						if (is_wp_error($delete)) {
							return $delete;
						}

						$this->num_discarded++;
						continue;
					}
				}
			}
			foreach ($resource['meta'] as $key=>$val) {
				update_post_meta($post['ID'], $key, (String)$val);
			}

			$result = wp_update_post($post, true);

			if (is_wp_error($result)) {
				return $result; // pass back wordpress error
			}

			if ($new) {
				$this->num_imports++;
			} else {
				$this->num_updates++;
			}

		}
		return true;
	}

	/**
	* Set featured image - copy of a stupid wordpress function 'media_sideload_image'
	* This version attaches it as a featured image.
	**/
	function set_featured_image($url, $post_id, $description)
	{
		if (empty($url)) {
			return false;
		}

		// Download file to temp location
		$tmp_path = download_url($url);

		// Set variables for storage
		// fix filename for query strings
		preg_match('/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $url, $matches);
		$file_info['name'] = basename($matches[0]);
		$file_info['tmp_name'] = $tmp_path;

		// If error storing temporarily, unlink
		if (is_wp_error($tmp_path)) {
			@unlink($tmp_path);
			$file_info['tmp_path']='';
			return false;
		}

		// do the validation and storage stuff
		$thumbnail_id = media_handle_sideload($file_info, $post_id, $description);

		// If error storing permenently, unlink
		if (is_wp_error($thumbnail_id)) {
			@unlink($tmp_path);
			return false;
		}

		return set_post_thumbnail($post_id, $thumbnail_id);

	}

	/* function checkposts()
	* check for posts that have been removed from caleb.
	*/
	function checkposts()
	{
		$passed = $failed = 0;
		if (!count($this->resources)) {
			$this->pull();
		}
		foreach ($this->resources as $resource) {
			$om = $resource->children('om', true);
			$ids[] = (string)$om->id;
		}
		$query = new wp_query(array(
					'posts_per_page'=>'-1',
					'meta_query' => array(
							array(
								'key' => '_caleb_type',
								'value' => 'resource',
								),
							array(
								'key' => '_caleb_id',
								'value' => $ids,
								'compare'=> 'not in'
								)
					)
		));

		$query->get_posts();

		while ($query->have_posts()) {
			$query->next_post();
			wp_trash_post($query->post->ID);
			$failed++;
		}
		$this->number_deleted = $failed;
		return $failed;

	}


	/**
	* deleteAll()
	* delete imported resources from caleb
	**/
	function deleteAll() {
		$i = 0;
		$query = new WP_Query("meta_key=_caleb_type&meta_value=resource&posts_per_page=-1");
		while ($query->have_posts()) {
			$query->next_post();
			$result = wp_trash_post($query->post->ID);

			if (is_wp_error($result)) {
				return $result;
			}

			$i++;
		}
		$this->num_deleted = $i;
		return true;
	}
}

function caleb_connect_resources_cron() {
	$getter = new CalebResourceGetter();
	$getter->runCron();
}
?>
