<?php
/*
* file: getter.php - pulls the jobs from Caleb and saves them as posts.
* @author Chris Young <chris.young@om.org>
*/

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
class CalebJobGetter
{
    var $base_url = OM_APP_JOBS_URL;
    var $single_baseurl = OM_APP_JOB_URL;
    var $jobs;
    var $posts;
    var $options;
    var $number_successful_imports = 0;
    var $number_successful_updates = 0;
    var $number_trashed = 0;

    function __construct()
    {
        include('job-options.php');

        // Get options and put the feed options into a nested array for ease of use.
        $this->options = wp_parse_args(get_option('caleb-connect-job_options'));
        for ($i=0; $i<$this->options['number_of_feeds']; $i++)
            foreach ($this->options as $k=>$v)
                if (strpos($k,(String)$i)==(strlen($k)-1))
                    $this->options['feeds'][$i][substr($k,0,-2)] = $v;
        $this->options = array_merge($this->options,wp_parse_args(get_option('caleb-connect-general_options')));
        foreach ($this->options['feeds'] as $feed_id=>$feed_options)
            foreach ($feed_options as $option_name=>$option_value)
                if ($result = unserialize($option_value)) $this->options['feeds'][$feed_id][$option_name] = $result;
    }

    /*
    * function getNow() - get immediately rather than via CRON job
    * returns integer - number of affected rows.
    */
    function getNow()
    {
        $result = $this->get();
        if (is_wp_error($result)) {
            foreach ($result->errors as $error_group=>$errors) {
                foreach ($errors as $msg) {
                    set_transient('caleb-connect-job-error',array(time(),$error_group,$msg));
                }
            }
            return false;
        }
        return array($this->number_successful_imports,$this->number_successful_updates);
    }

    /*
    * function runCron() - set up the CRON job
    * obeys "RSS Feed Cache Timeout" setting.
    */
    function runCron()
    {
        $result = $this->get();

        if (is_wp_error($result)) {
            foreach ($result->errors as $error_group=>$errors) {
                foreach ($errors as $msg) {
                    set_transient('caleb-connect-job-error',array(time(),$error_group,$msg));
                }
            }
            return false;
        }
        else
        {
            set_transient('caleb-connect-job-message',array(time(),'import',$this->number_successful_imports.' jobs imported as posts. '.$this->number_successful_updates.' jobs updated.'));
            return true;
        }
    }

    /*
    * function: get() - pull and save posts
    * returns integer - number of affected rows
    */
    function get()
    {
        $max_posts = 100;
        $result = $this->pull();
        if ($result) return $result;

        // Check to see if any posts exist that are not in our list
        $result = $this->checkPosts();
        if (is_wp_error($result)) return $result;
        else $this->number_trashed = $result;
        
        // Import the jobs in reverse date order - don't just do the first feed first!
        foreach ($this->jobs as $key => $row)
            $sort[$key] = strtotime($row->pubDate);
        array_multisort($sort,SORT_DESC,$this->jobs);

        foreach($this->jobs as $item) {
            $om = $item->children('om',true);
            $query = new WP_Query( 'post_type=caleb_job&meta_key=_caleb_id&meta_value='.$om->id );
            if ($query->have_posts()) {
                $post = $query->posts[0];
                if ($item->pubDate != get_post_meta($post->ID,'_caleb_pubDate',true)) {
                    if ($job = $this->getJob($om->id)) {
                        $job['post_id'] = $post->ID;
                        $this->posts[] = $job;
                    }
                }
            } else {
                if ($job = $this->getJob($om->id)) {
                    $job['feed_id'] = $item['feed_id'];
                    $this->posts[] = $job;
                }
            }
            if (count($this->posts) >= $max_posts) break;
        }
        $result = $this->save();
        return $result;
    }

    /*
    * function pull() - actually request the jobs from caleb
    * returns: array of jobs from caleb
    */
    function pull()
    {
        $options = $this->options; 

        for ($i=0;$i<$options['number_of_feeds'];$i++)
        {
            $feed_opt = $this->options['feeds'][$i];
            $feed_url = $this->base_url."?";
            if (count($feed_opt['country'])) foreach ($feed_opt['country'] as $country) if ($country) $feed_url .= '&country='.$country;
            if (count($feed_opt['category'])) foreach ($feed_opt['category'] as $category) if ($category) $feed_url .= '&category='.$category;
            if (count($feed_opt['region'])) foreach ($feed_opt['region'] as $region) if ($region) $feed_url .= '&region='.$region;
            if ($options["field_id"]) $feed_url .= '&field='.$options["field_id"];
            if ($options["pin"]) $feed_url .= '&pin='.$options["pin"];
            $feed_url .= "&length=5000";

            $url = html_entity_decode($feed_url);
            $response = wp_remote_get($url, array('timeout' => 15));
            if (!is_wp_error($response)) {
                $data = wp_remote_retrieve_body($response);
            }
            
            $data = simplexml_load_string($data);
            foreach($data->channel->item as $job) {
                $job['feed_id'] = $i;
                $this->jobs[] = $job; 
            }
        }
    }

    /*
    * function: getJob(String $job_id)
    * Get a single job from caleb using the ID string
    * Returns an array representing the job
    */
    function getJob($job_id)
    {
        $url = html_entity_decode($this->single_baseurl."?jobId=".$job_id);
        
        $response = wp_remote_get($url, array('timeout' => 15));
        if (!is_wp_error($response)) {
            $data = wp_remote_retrieve_body($response);
        }

        $data = simplexml_load_string($data);
        // Test to see if an actual item is returned.
        if (!$data->channel->item) return false;
        $item = $data->channel->item;

        $om = $item->children('om',true);
        foreach ($om->categories as $category)
            $categories[] = (String)$category;
        $categories = implode(', ',$categories);
        $job = array(
            'title'=>(String)$item->title,
            'meta'=>array(
                'Country'=>$om->country,
                'Hours'=>$om->fullPartTime,
                'Start Date'=>$om->startDate,
                'End Date'=>$om->endDate,
                'Commitment Length'=>$om->commitmentLength,
                '_link'=>$item->link,
                '_caleb_id'=>$om->id,
                '_caleb_type'=>'opportunity',
                '_caleb_pubDate'=>$item->pubDate,
                ),
            'requirements'=>(String)$om->requirements,
            'description'=>(String)$om->description,
            'categories'=>$categories
            );
        return $job;
    }

    /*
    * function save() - save new jobs as posts in wordpress
    * returns integer - number of rows inserted
    */
    function save()
    {
        $number_successful_imports = 0;
        $number_successful_updates = 0;
        
        if (!is_array($this->posts)) return true;

        foreach ($this->posts as $item)
        {
            $i = (int)$item['feed_id'];
            if ($item['post_id']) {
                $post = $item;
                $post['ID'] = $item['post_id'];
                $new = false;
            } else {
                $new = true;
                $post = array(
                    'post_name'     => sanitize_title($item['title'].'-'.(String)$item['meta']['_caleb_id']),
                    'tax_input'     => array('job_group'=>$this->options['feeds'][$i]['wp_category'],
                                        'job_skills' => $item['categories'],
                                        'job_tags' => $this->options['feeds'][$i]['wp_tags']), 
                    'post_status'   => $this->options['feeds'][$i]['wp_status'], 
                    'comment_status'=> $this->options['feeds'][$i]['wp_comment_status'],
                    'post_title'    => $item['title'],
                    'post_date'     => date('Y-m-d H:i:s',strtotime($item['meta']['_caleb_pubDate'])),
                );
                $post['ID'] = wp_insert_post($post,true);
                if (is_wp_error($post['ID'])) return $post['ID'];
            }

            $meta = "";
            foreach ($item['meta'] as $key=>$val)
                if ($val && substr($key,0,1)!='_') $meta .= "<b>$key:</b> $val<br/>";
 
            foreach ($item['meta'] as $key=>$val)
                update_post_meta($post['ID'],$key,(String)$val);

            $post = array(
                'ID'            => $post['ID'],
                'post_type'     => 'caleb_job',
                'post_content'  => "<div class='om_job_meta'><p>$meta</p></div>"
                                  ."<div class='om_job_description'><p>".$item['description']."</p></div>"
                                  ."<div class='om_job_requirements'><h2>Requirements:</h2>".$item['requirements']."</div>",
                'post_excerpt'  => strip_tags(html_entity_decode($item['description'])),
                'post_date'     => date('Y-m-d H:i:s',strtotime($item['meta']['_caleb_pubDate'])),
                'edit_date'     => true,
                'post_title'    => $item['title'],
                );

            $result = wp_update_post($post,true);
            if (is_wp_error($result)) return $result; // pass back wordpress error
            else if ($new) $number_successful_imports++;
            else $number_successful_updates++;

        }
        $this->number_successful_imports = $number_successful_imports;
        $this->number_successful_updates = $number_successful_updates;
        return true;
    }

    /* function checkPosts()
    * check for posts that have been removed from caleb.
    */
    function checkPosts()
    {
        $passed = $faled = 0;
        if (!count($this->jobs)) $this->pull();
        foreach ($this->jobs as $job)
        {
            $om = $job->children('om',true);
            $ids[] = (String)$om->id;
        }
        $query = new WP_Query(array(
                    'posts_per_page'=>'-1',
                    'post_type'=>'caleb_job',
                    'meta_query' => array(
                            array(
                                'key' => '_caleb_type',
                                'value' => 'opportunity',
                                ),
                            array(
                                'key' => '_caleb_id',
                                'value' => $ids,
                                'compare'=> 'NOT IN'
                                )
                    )
        ));
        $query->get_posts();
        while ($query->have_posts())
        {
            $query->next_post();
            wp_trash_post($query->post);
            $failed++;
        }
        $this->num_trashed = $failed;
        return $failed;

    }

    /**
    * trashAll() 
    * trash all imported jobs from caleb
    **/
    function trashAll()
    {
        $i = 0;
        $query = new WP_Query("post_type=caleb_job&posts_per_page=-1");
        while ($query->have_posts()) {
            $query->next_post();
            if ($result = wp_trash_post($query->post->ID)) $i++;
            else if (is_wp_error($result)) die(print_r($result)); //return false;
        }
        return $i;
    }
}

function caleb_connect_jobs_cron()
{
    $jobGetter = new CalebJobGetter();
    $jobGetter->runCron();
}

add_action('init','create_job_taxonomies');
function create_job_taxonomies()
{
    register_taxonomy(
        'job_group',
        'caleb_job',
        array(
            'hierarchical'=>true,
            'show_admin_column'=>true,
            'labels'=> array(
                'name'=> __('Groups'),
                'menu_name'=> __('Job Groups'),
                'singular_name'=> __('Group')
            ),
            'rewrite'=>array('slug'=>__('jobs-by-group')),
        )
    );
    register_taxonomy(
        'job_tags',
        'caleb_job',
        array(
            'hierarchical'=>false,
            'show_admin_column'=>true,
            'labels'=> array(
                'name'=> __('Tags'),
                'menu_name'=> __('Job Tags'),
            ),
            'rewrite'=>array('slug'=>__('jobs-by-tag')),
        )
    );
    register_taxonomy(
        'job_skills',
        'caleb_job',
        array(
            'hierarchical'=>false, // When this gets set to true - we need to prepopulate the list before importing
            'show_admin_column'=>true,
            'labels'=> array(
                'name'=> __('Skills'),
            ),
            'rewrite'=>array('slug'=>__('by-skill')),
        )
    );
}
add_action('init','create_post_type');
function create_post_type()
{
    register_post_type('caleb_job',
        array(
            'label'=>'Jobs',
            'public'=>true,
            'has_archive'=>true,
            'supports'=>array(
                'title','editor','excerpt','custom-fields','revisions'
            ),
            'menu_icon'=>'dashicons-id',
            'rewrite'=>array('slug'=>__('jobs')),
        )
    );
    register_taxonomy_for_object_type('job_group','caleb_job');
    register_taxonomy_for_object_type('job_tags','caleb_job');
    register_taxonomy_for_object_type('job_skills','caleb_job');
}

add_action('loop_end','print_caleb_job_bit');
function print_caleb_job_bit()
{
    global $wp, $wp_query;
    if ((isset($wp_query->post))&&($wp_query->post->post_type == 'caleb_job')) {
	    echo "<span class='disclaimer'>",
		 __("Please note: All jobs/opportunities on this site are unsalaried. Most people joining OM have to raise financial support to cover their living expenses, usually through gifts from home churches and other supporters"),
                 "</span>";
    }
}

?>
