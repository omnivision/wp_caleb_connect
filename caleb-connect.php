<?php
/*
Plugin Name: OM Caleb Connect 
Description: Plugin to periodically download resources from Caleb to add as posts. 
Version: 0.0.6
Text Domain: caleb_connect
Author: OMNIvision
Author URI: http://omnivision.om.org/
License: GPL2
*/
/*  Copyright 2014  OMNIvision  (email : webmaster.omnivision@om.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define('OM_APP_URL','http://app.om.org/rss/');
define('OM_APP_RESOURCE_URL',OM_APP_URL.'resource.rss');
define('OM_APP_RESOURCES_URL',OM_APP_URL.'resources.rss');
define('OM_APP_STMS_URL',OM_APP_URL.'challenge.rss');
define('OM_APP_STM_URL',OM_APP_URL.'optioninfo.rss');
define('OM_APP_JOBS_URL',OM_APP_URL.'opportunities.rss');
define('OM_APP_JOB_URL',OM_APP_URL.'opportunity.rss');

register_deactivation_hook(__FILE__, 'caleb_connect_deactivation');

// Require the functions for getting resources to save as posts
require_once('jobs-widget.php');
require_once('resource-getter.php');
require_once('job-getter.php');
require_once('stm-getter.php');

if( is_admin() ) {
    include_once('settings.php');
    include_once('resource-settings.php');
    include_once('job-settings.php');
    include_once('stm-settings.php');
}
add_action ('caleb_connect_resources_cron','caleb_connect_resources_cron');
add_action ('caleb_connect_jobs_cron','caleb_connect_jobs_cron');
add_action ('caleb_connect_STMs_cron','caleb_connect_STMs_cron');

function caleb_connect_deactivation() {
    wp_clear_scheduled_hook('caleb_connect_resources_cron');
    wp_clear_scheduled_hook('caleb_connect_jobs_cron');
    wp_clear_scheduled_hook('caleb_connect_STMs_cron');
}
