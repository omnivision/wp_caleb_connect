<?php
$caleb_tags = array(
    "categories"=>array(
        02 => "Sports",
        03 => "Arts Performing - Dance, Drama & Mime",
        05 => "Arts Performing - Music & Sound ",
        06 => "Creative Ministries - Other",
        07 => "Creative Ministries - Puppets & Games",
        09 => "Literature & Video Distribution",
        10 => "Arts Media - Video & Photography ",
        11 => "Friendship Evangelism ",
        12 => "Coffee Bars",
        13 => "Door To Door",
        14 => "Teaching English ",
        15 => "Cultural, International Events ",
        16 => "Camps",
        17 => "Orphanages",
        18 => "Drug Rehabilitation ",
        19 => "Prison Ministry",
        20 => "HiV, Aids Ministry",
        21 => "Travelling Teams",
        22 => "Prayer",
        23 => "Ship ",
        24 => "Construction/Building Maintenance",
        25 => "Community Projects/Social Action ",
        28 => "Reaching to: Children",
        29 => "Reaching to: Youth",
        30 => "Reaching to: Teens",
        31 => "Reaching to: College/University",
        32 => "Reaching to: Ethnic Peoples",
        33 => "Reaching to: Other Faiths ",
        36 => "Reaching to: Homeless/Street People",
        37 => "Sex Trade Workers",
        39 => "Medical",
        40 => "Teens In Mission ",
        41 => "Transform",
        42 => "Arts Visual - Painting & Drawing ",
        43 => "Other",
        44 => "IT",
    ),
    "countries"=>array(
			"CEN"=>"Afghanistan and Central Asia", "ALQ"=>"Albania", "NOR"=>"Algeria and North Africa", "ASQ"=>"American Samoa", "ADQ"=>"Andorra", "AOQ"=>"Angola", "AIQ"=>"Anguilla", "AGQ"=>"Antigua &amp; Barbuda", "ARQ"=>"Argentina", "CAU"=>"Armenia and Caucasus", "AWQ"=>"Aruba", "AUQ"=>"Australia", "ATQ"=>"Austria", "CAU"=>"Azerbaijan and Caucasus", "BSQ"=>"Bahamas", "ARA"=>"Bahrain and Arabian Peninsula", "BDQ"=>"Bangladesh", "BBQ"=>"Barbados", "BYQ"=>"Belarus", "BEQ"=>"Belgium", "BZQ"=>"Belize", "BJQ"=>"Benin", "BMQ"=>"Bermuda", "BTQ"=>"Bhutan", "BOQ"=>"Bolivia", "BQQ"=>"Bonaire, Sint Eustatius &amp; Saba", "BAQ"=>"Bosnia &amp; Hercegovina", "BWQ"=>"Botswana", "BVQ"=>"Bouvet Is.", "BRQ"=>"Brazil", "IOQ"=>"British Indian Ocean Territory", "BNQ"=>"Brunei Darussalam", "BGQ"=>"Bulgaria", "BFQ"=>"Burkina Faso", "BIQ"=>"Burundi", "KHQ"=>"Cambodia", "CMQ"=>"Cameroon", "CAQ"=>"Canada", "CVQ"=>"Cape Verde", "KYQ"=>"Cayman Is.", "CFQ"=>"Central African Republic", "AFR"=>"Chad and Africa", "CLQ"=>"Chile", "FAR"=>"China and Far East", "CXQ"=>"Christmas Is.", "CCQ"=>"Cocos Is.", "COQ"=>"Colombia", "KMQ"=>"Comoros", "CGQ"=>"Congo", "CDQ"=>"Congo, The Democratic Rep", "CKQ"=>"Cook Is.", "CRQ"=>"Costa Rica", "HRQ"=>"Croatia", "CUQ"=>"Cuba", "CWQ"=>"Curaçao", "CYQ"=>"Cyprus", "CZQ"=>"Czech Republic", "DKQ"=>"Denmark", "EAS"=>"Djibouti and East Africa", "DMQ"=>"Dominica", "DOQ"=>"Dominican Republic", "TLQ"=>"East Timor", "ECQ"=>"Ecuador", "EGQ"=>"Egypt", "SVQ"=>"El Salvador", "GQQ"=>"Equatorial Guinea", "ERQ"=>"Eritrea", "EEQ"=>"Estonia", "ETQ"=>"Ethiopia", "FKQ"=>"Falkland Is.", "FOQ"=>"Faroe Is.", "FJQ"=>"Fiji", "FIQ"=>"Finland", "FRQ"=>"France", "GFQ"=>"French Guiana", "PFQ"=>"French Polynesia", "TFQ"=>"French Southern Territories", "GAQ"=>"Gabon", "GMQ"=>"Gambia", "CAU"=>"Georgia and Caucasus", "DEQ"=>"Germany", "GHQ"=>"Ghana", "GIQ"=>"Gibraltar", "GRQ"=>"Greece", "GLQ"=>"Greenland", "GDQ"=>"Grenada", "GPQ"=>"Guadeloupe", "GUQ"=>"Guam", "GTQ"=>"Guatemala", "GGQ"=>"Guernsey", "GNQ"=>"Guinea", "GWQ"=>"Guinea-Bissau", "GYQ"=>"Guyana", "HTQ"=>"Haiti", "HMQ"=>"Heard &amp; McDonald Is.", "VAQ"=>"Holy See (Vatican City State)", "HNQ"=>"Honduras", "HKQ"=>"Hong Kong", "HUQ"=>"Hungary", "ISQ"=>"Iceland", "INQ"=>"India", "IDQ"=>"Indonesia", "QOQ"=>"International", "IRN"=>"Iran", "NEA"=>"Iraq and Near East", "IEQ"=>"Ireland", "IMQ"=>"Isle of Man", "ILQ"=>"Israel", "ITQ"=>"Italy", "CIQ"=>"Ivory Coast", "JMQ"=>"Jamaica", "JPQ"=>"Japan", "JEQ"=>"Jersey", "NEA"=>"Jordan and Near East", "CEN"=>"Kazakhstan and Central Asia", "KEQ"=>"Kenya", "KIQ"=>"Kiribati", "KOR"=>"Korea", "KRQ"=>"Korea, Republic of", "QMQ"=>"Kosovo", "ARA"=>"Kuwait and Arabian Peninsula", "CEN"=>"Kyrgyzstan and Central Asia", "LAO"=>"Laos", "LVQ"=>"Latvia", "NEA"=>"Lebanon and Near East", "LSQ"=>"Lesotho", "LRQ"=>"Liberia", "NOR"=>"Libya and North Africa", "LIQ"=>"Liechtenstein", "LTQ"=>"Lithuania", "LUQ"=>"Luxembourg", "MOQ"=>"Macau", "MKQ"=>"Macedonia", "MGQ"=>"Madagascar", "MWQ"=>"Malawi", 
            "MYQ"=>"Malaysia", "MVQ"=>"Maldives", "MLQ"=>"Mali", "MTQ"=>"Malta", "MHQ"=>"Marshall Is.", "MQQ"=>"Martinique", "NOR"=>"Mauritania and North Africa", "MUQ"=>"Mauritius", "YTQ"=>"Mayotte", "MXQ"=>"Mexico", "FMQ"=>"Micronesia, Fed States", "MDQ"=>"Moldova, Republic of", "MCQ"=>"Monaco", "MNQ"=>"Mongolia", "MEQ"=>"Montenegro", "MSQ"=>"Montserrat", "NOR"=>"Morocco and North Africa", "MZQ"=>"Mozambique", "MMQ"=>"Myanmar", "NAQ"=>"Namibia", "NRQ"=>"Nauru", "NPQ"=>"Nepal", "NLQ"=>"Netherlands", "NCQ"=>"New Caledonia", "NZQ"=>"New Zealand", "NIQ"=>"Nicaragua", "NEQ"=>"Niger", "NGQ"=>"Nigeria", "NUQ"=>"Niue", "NFQ"=>"Norfolk Is.", "MPQ"=>"Northern Mariana Is.", "NOQ"=>"Norway", "ARA"=>"Oman and Arabian Peninsula", "PKQ"=>"Pakistan", "PWQ"=>"Palau", "PSQ"=>"Palestine, State of", "PAQ"=>"Panama", "PGQ"=>"Papua New Guinea", "PYQ"=>"Paraguay", "PEQ"=>"Peru", "PHQ"=>"Philippines", "PNQ"=>"Pitcairn", "PLQ"=>"Poland", "PTQ"=>"Portugal", "PRQ"=>"Puerto Rico", "ARA"=>"Qatar and Arabian Peninsula", "REQ"=>"Reunion", "ROQ"=>"Romania", "RUQ"=>"Russian Federation", "RWQ"=>"Rwanda", "BLQ"=>"Saint Barthélemy", "SHQ"=>"Saint Helena", "KNQ"=>"Saint Kitts &amp; Nevis", "LCQ"=>"Saint Lucia", "MFQ"=>"Saint Martin (French part)", "PMQ"=>"Saint Pierre &amp; Miquelon", "VCQ"=>"Saint Vincent &amp; the Grenadines", "WSQ"=>"Samoa", "SMQ"=>"San Marino", "STQ"=>"Sao Tome &amp; Principe", "ARA"=>"Saudi Arabia and Arabian Peninsula", "SNQ"=>"Senegal", "RSQ"=>"Serbia", "SCQ"=>"Seychelles Is.", "QQQ"=>"Ships", "SLQ"=>"Sierra Leone", "SGQ"=>"Singapore", "SXQ"=>"Sint Maarten (Dutch part)", "SKQ"=>"Slovakia", "SIQ"=>"Slovenia", "SBQ"=>"Solomon Is.", "SOQ"=>"Somalia", "ZAQ"=>"South Africa", "GSQ"=>"South Georgia &amp; Sandwich Is.", "SSQ"=>"South Sudan, Republic of and Middle East", "ESQ"=>"Spain", "LKQ"=>"Sri Lanka", "MID"=>"Sudan and Middle East", "SRQ"=>"Suriname", "SJQ"=>"Svalbard &amp; Jan Mayen", "SZQ"=>"Swaziland", "SEQ"=>"Sweden", "CHQ"=>"Switzerland", "NEA"=>"Syria and Near East", "TWQ"=>"Taiwan", "CEN"=>"Tajikistan and Central Asia", "TZQ"=>"Tanzania", "THQ"=>"Thailand", "TGQ"=>"Togo", "TKQ"=>"Tokelau", "TOQ"=>"Tonga", "TTQ"=>"Trinidad &amp; Tobago", "NOR"=>"Tunisia and North Africa", "TRQ"=>"Turkey", "CEN"=>"Turkmenistan and Central Asia", "TCQ"=>"Turks &amp; Caicos Is.", "TVQ"=>"Tuvalu", "ARA"=>"UAE and Arabian Peninsula", "UNI"=>"US Minor Outlying Islands", "UGQ"=>"Uganda", "UAQ"=>"Ukraine", "GBQ"=>"United Kingdom", "USQ"=>"United States", "UYQ"=>"Uruguay", "CEN"=>"Uzbekistan and Central Asia", "VUQ"=>"Vanuatu", "VEQ"=>"Venezuela", "VNQ"=>"Vietnam", "VIQ"=>"Virgin Is. US", "VGQ"=>"Virgin Is., British", "WFQ"=>"Wallis &amp; Futuna", "NOR"=>"Western Sahara and North Africa", "ARA"=>"Yemen and Arabian Peninsula", "ZMQ"=>"Zambia", "ZWQ"=>"Zimbabwe", "AXQ"=>"Åland Islands"
    ),
);
$wp_statuses = array (
            'publish'=>'Published',
            'private'=>'Private',
            'pending'=>'Pending',
            'draft'=>'Draft',
            );
$wp_comment_statuses = array (
            'closed'=>'Closed',
            'open'=>'Open',);
