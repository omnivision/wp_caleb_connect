Caleb Connect for Wordpress
=====================
Take fantastic existing articles from Caleb automatically and add them as posts.
Also import job opportunities
You can auto-approve them, or wait for approval.

* _Requires at least Wordpress:_ 3.0.1
* _Tested up to:_ 4.0.0
* _Stable tag:_ 0.0.6
* _License:_ GPLv2 or later
* _License URI:_ <http://www.gnu.org/licenses/gpl-2.0.html>

## Installation

1. Download the latest build from the Downloads Page. Check in Tags.
2. Upload `wp_caleb_connect` to the `/wp-content/plugins/` directory
or Upload it using Wordpress' upload new plugin page
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Under Posts, Jobs and STMs you'll find Caleb Connect. Click that
5. Choose to have at least 1 feed. Choose some options. Click Download to see.
6. Click 'Activate' to schedule a daily task to update the information.
7. Add the posts to your website through the menu editor.

## Frequently Asked Questions

*Q: Does this plugin cache articles?*

Yes of course it does. This plugin adds caleb resources as posts. That's about as cached as you can get. Feel free to use cache enhancing plugins alongside this to make it even better. It'll run daily to update the posts.

*Q: Can I edit the articles before they go live?*

A: Yes you can! You can tell CC to import as Drafts and then you can modify the articles before they appear on the site. Do nothing and nothing happens. Alternatively you can have articles automatically published and edit them later.

*Q: How can I help fix your plugin / make it better?*

A: You could start by submitting an Issue in bitbucket. If you like to code yourself, try forking the project and when you've got it fixed or impoved, send us a pull request.

## Changelog

* 0.0.6
Added WPML support for resources (articles, etc). (Multi-linugal)

* 0.0.5
Custom post type for Jobs + custom taxonomies
Moved settings pages to sit with their post type
Download more than fifteen items at a time. Now 50 resources, 100 jobs
Drop posts with no thumbnail - if you chose to do that
Multiselect boxes for Category,Country,Region,MediaType

* 0.0.4
Bugfixes and code restructuring

* 0.0.3
added the ability to delete everything.

* 0.0.2
A added jobs functionality

* 0.0.1
A Prototype

