<?php
class CalebConnectSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    public $feeds;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Caleb Connect :: General Settings',
            'Caleb Connect',
            'manage_options',
            'caleb-connect-general',
            array( $this, 'create_admin_page' )
        );

        // display admin notice when cache has been cleared
        add_action( "admin_notices", array ( $this, 'parse_message' ) );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'caleb-connect-general_options' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Caleb Connect :: General Settings</h2>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'caleb_connect_general_option_group' );
                do_settings_sections( 'caleb-connect-general' );

                submit_button("Update Settings");
            ?>
            </form>

        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        if (!is_array($this->feeds)) {
            $this->feeds = array();
        }

        $this->feeds[] = array("type"=>4,"countryId"=>"AU");

        register_setting(
            'caleb_connect_general_option_group', // Option group
            'caleb-connect-general_options', // Option name
            array( $this, 'sanitize' ) // Sanitize (callback)
        );

        add_settings_section(
            'caleb-connect_general', // ID
            'General Settings', // Title
            array( $this, 'general_section_info' ), // Callback
            'caleb-connect-general' // Page
        );

        add_settings_field(
            'field_id', // ID
            'Field ID (optional)', // Title
            array( $this, 'field_id_callback' ), // Callback
            'caleb-connect-general', // Page
            'caleb-connect_general' // Section
        );

        add_settings_field(
            'pin', // ID
            'Field PIN (optional)', // Title
            array( $this, 'pin_callback' ), // Callback
            'caleb-connect-general', // Page
            'caleb-connect_general' // Section
        );

        add_settings_field(
            'add_qtranslate_flags', // ID
            'Set QTranslate Language for posts', // Title
            array( $this, 'add_qtranslate_callback'), // Callback
            'caleb-connect-general', // Page
            'caleb-connect_general' // Section
        );

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $sanitized_input = array();

        if (isset($input['pin']) && $input['pin'] != '') {
            $sanitized_input['pin'] = sanitize_text_field($input['pin']);
        }


        if (isset($input['field_id'])) {
            $sanitized_input['field_id'] = sanitize_text_field( $input['field_id'] );
        }

        if (isset($input['add_qtranslate_flags'])) {
            $sanitized_input['add_qtranslate_flags'] = ($input['add_qtranslate_flags'] == '1'?'1':'0');
        }

        return $sanitized_input;
    }

    /**
     * Print the Section header text
     */
    public function general_section_info()
    {
        print 'Enter your settings below for this plugin. Not all fields are required and the defaults will work.';
    }

    /**
     * Print the Section header text
     */
    public function feed_section_info()
    {
        print 'Choose what general you want and where you want them to go.';
    }

    public function pin_callback()
    {
        printf(
            '<input type="text" id="pin" name="caleb-connect-general_options[pin]" value="%s" />',
            isset( $this->options['pin'] ) ? esc_attr( $this->options['pin']) : ''
        );
    }

    public function field_id_callback()
    {
        printf(
            '<input type="text" id="Field ID" name="caleb-connect-general_options[field_id]" value="%s" />',
            isset( $this->options['field_id'] ) ? esc_attr( $this->options['field_id']) : ''
        );
    }

    public function add_qtranslate_callback()
    {
        printf(
            '<input type="checkbox" id="Add QTranslate Flags"'
            . ' name="caleb-connect-general_options[add_qtranslate_flags]" value="1" %s />',
            ($this->options['add_qtranslate_flags'] == '1' ? 'checked' : 'unchecked')
        );
    }


    /**
    * Parses the URL field to determine if the cache was cleared and prints to screen if so.
    */
    public function parse_message()
    {
    }

}

if( is_admin() ) {
    $om_settings_page = new CalebConnectSettingsPage();
}
?>
