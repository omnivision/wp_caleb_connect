<?php  
class CalebSTMsSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_submenu_page(
            'edit.php?post_type=caleb_stm',
            'Caleb Connect :: Caleb Sort Term Missions', 
            'Caleb Connect', 
            'manage_options', 
            'caleb-connect-STMs', 
            array( $this, 'create_admin_page' )
        );

        // display admin notices
        add_action( "admin_notices", array ( $this, 'parse_message' ) );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'caleb-connect-STM_options' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Caleb Connect :: Caleb Sort Term Missions</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'caleb_connect_STMs_option_group' );   
                do_settings_sections( 'caleb-connect-STMs' );

                submit_button("Update Settings"); 
            ?>
            </form>

            <h3>Scheduled Sort Term Mission Imports</h3>
            Currently, daily STM imports are <?php echo ( wp_next_scheduled('caleb_connect_STMs_cron') ? 'indeed' : 'not' ); ?> scheduled.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                 <?php if (wp_next_scheduled('caleb_connect_STMs_cron')) : ?> 
                <input type="hidden" name="action" value="caleb_connect_STMs_cron_deactivate" />
                <input type="submit" name="button_deactivate" value="Deactivate" class="button" />
                <?php else : ?>
                <input type="hidden" name="action" value="caleb_connect_STMs_cron_activate" />
                <input type="submit" name="button_activate" value="Activate" class="button" />
                <?php endif; ?>
            </form>
            </p>
            <h3>Do it now</h3>
            Download the STMs now, rather than waiting.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="get_STMs" />
                <input type="submit" name="button_get_STMs" value="Download" class="button" />
            </form>
            </p>
            <h3>Check Posts</h3>
            Check for posts that are no longer on caleb.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="check_caleb_STMs" />
                <input type="submit" name="button_check_posts" value="Check" class="button" />
            </form>
            </p>
            <h3>Trash Sort Term Missions</h3>
            Trash all the imported STMs now.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="trash_STMs_now" />
                <input type="submit" name="button_trash_STMs" value="Trash" class="button" />
            </form>
            </p>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        include('stm-options.php');

        register_setting(
            'caleb_connect_STMs_option_group', // Option group
            'caleb-connect-STM_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'caleb-connect_general', // ID
            'General Settings', // Title
            array( $this, 'general_section_info' ), // Callback
            'caleb-connect-STMs' // Page
        );  
        
        add_settings_field(
            'from_country', 
            'From Country (2 letter ISO)', 
            array( $this, 'from_country_callback' ), 
            'caleb-connect-STMs', 
            'caleb-connect_general'
        );
                    
        add_settings_field(
            'number_of_feeds', // ID
            'Number of Feeds', // Title 
            array( $this, 'number_of_feeds_callback' ), // Callback
            'caleb-connect-STMs', // Page
            'caleb-connect_general' // Section           
        );
        $this->options = get_option( 'caleb-connect-STM_options' );
        for ($i=0;$i<$this->options['number_of_feeds'];$i++) {

            add_settings_section(
                'caleb-connect_feed'.$i, // ID
                'Feed '.($i+1), // Title
                array( $this, 'feed_section_info' ), // Callback
                'caleb-connect-STMs' // Page
            );  
            
            add_settings_field(
                'country', // ID
                'Country', // Title 
                array($this,'country_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'category', // ID
                'Caleb Category', // Title 
                array($this,'category_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'languages', // ID
                'Languages (comma seperated)', // Title 
                array($this,'languages_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'families', // ID
                'Families Accepted', // Title 
                array($this,'families_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'groups', // ID
                'Groups Accepted', // Title 
                array($this,'groups_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'married', // ID
                'Married Couples Accepted', // Title 
                array($this,'married_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'age', // ID
                'Participant Age (one only)', // Title 
                array($this,'age_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'pastdDays', // ID
                'Past Days', // Title 
                array($this,'pastDays_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_category', // ID
                'STM Group (Used to sort in Wordpress)', // Title 
                array($this,'wp_category_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_tags', // ID
                'Wordpress Tags (comma separated)', // Title 
                array($this,'wp_tags_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_status', // ID
                'Wordpress Status', // Title 
                array($this,'wp_status_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'wp_statuses'=>$wp_statuses) // Args for callback           
            );
            add_settings_field(
                'wp_comment_status', // ID
                'Wordpress Comment Status', // Title 
                array($this,'wp_comment_status_callback'), // Callback
                'caleb-connect-STMs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'wp_comment_statuses'=>$wp_comment_statuses) // Args for callback           
            );
        }

        add_action('admin_post_get_STMs', 'get_STMs');
        add_action('admin_post_trash_STMs_now', 'trash_STMs');
        add_action('admin_post_check_caleb_STMs', 'caleb_connect_STMs_check_posts');
        add_action('admin_post_caleb_connect_STMs_cron_activate', 'caleb_connect_STMs_cron_activate');
        add_action('admin_post_caleb_connect_STMs_cron_deactivate', 'caleb_connect_STMs_cron_deactivate');
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

        if( isset( $input['from_country'] ) )
            $new_input['from_country'] = substr(sanitize_text_field( $input['from_country'] ),0,2);

        if( isset( $input['number_of_feeds'] ) )
            // validate data and always be at least 1 feed.  
            $input['number_of_feeds'] = absint($input['number_of_feeds']);
            $new_input['number_of_feeds'] = ($input['number_of_feeds'] > 0 ? $input['number_of_feeds'] : 1);

        for ($i=0;$i<$this->options['number_of_feeds'];$i++)
        {
            if( isset( $input['country_'.$i] ) )
                $new_input['country_'.$i] = (Int)$input['country_'.$i];
            if( isset( $input['category_'.$i] ) )
                $new_input['category_'.$i] = (Int)$input['category_'.$i];
            if( isset( $input['languages_'.$i] ) )
                $new_input['languages_'.$i] = sanitize_text_field($input['languages_'.$i]);
            if( isset( $input['families_'.$i] ) )
                $new_input['families_'.$i] = $input['families_'.$i];
            if( isset( $input['groups_'.$i] ) )
                $new_input['groups_'.$i] = $input['groups_'.$i];
            if( isset( $input['married_'.$i] ) )
                $new_input['married_'.$i] = $input['married_'.$i];
            if( isset( $input['age_'.$i] ) )
                $new_input['age_'.$i] = (Int)$input['age_'.$i];
            if( isset( $input['pastDays_'.$i] ) )
                $new_input['pastDays_'.$i] = (Int)$input['pastDays_'.$i];
            if( isset( $input['wp_category_'.$i] ) )
                $new_input['wp_category_'.$i] = (Int)$input['wp_category_'.$i];
            if( isset( $input['wp_tags_'.$i] ) )
                $new_input['wp_tags_'.$i] = sanitize_text_field($input['wp_tags_'.$i]);
            if( isset( $input['wp_status_'.$i] ) )
                $new_input['wp_status_'.$i] = $input['wp_status_'.$i];
            if( isset( $input['wp_comment_status_'.$i] ) )
                $new_input['wp_comment_status_'.$i] = $input['wp_comment_status_'.$i];
        }

        return $new_input;
    }

    /** 
     * Print the Section header text
     */
    public function general_section_info()
    {
        print 'Enter your settings below for this plugin. Not all fields are required and the defaults will work.';
    }

    /** 
     * Print the Section header text
     */
    public function feed_section_info()
    {
        print 'Choose what STMs you want and where you want them to go.';
    }

    public function from_country_callback()
    {
        printf(
            '<input type="text" id="Field ID" name="caleb-connect-STM_options[from_country]" value="%s" />',
            isset( $this->options['from_country'] ) ? esc_attr( $this->options['from_country']) : ''
        );
    }

    public function number_of_feeds_callback()
    {
        printf(
            '<input type="text" id="number_of_feeds" name="caleb-connect-STM_options[number_of_feeds]" value="%s" />',
            isset( $this->options['number_of_feeds'] ) ? esc_attr( $this->options['number_of_feeds']) : ''
        );
    }

    public function country_callback($args)
    {
        $i = $args['i'];
        $countries = explode(',',$this->options['country_'.$i]);
            echo "<select id='country_$i' name='caleb-connect-STM_options[country_$i]'>";
            echo "<option value=''".(count($countries)<1 ? " selected":"").">Any</option>";
            foreach ($args['caleb_tags']['countries'] as $key=>$value) {
			   echo "<option value='$key'"; if (in_array($key,$countries)) echo " selected"; echo ">$value</option>>"; 
            }
          echo "</select>";
    }

    public function languages_callback($args)
    {
        $i = $args['i'];
        printf(
            '<input type="text" id="languages_'.$i.'" name="caleb-connect-STM_options[languages_'.$i.']" value="%s" />',
            isset( $this->options["languages_$i"] ) ? esc_attr( $this->options["languages_$i"]) : ''
        );
    }

    public function families_callback($args)
    {
        $i = $args['i'];
        echo "<input type='radio' name='caleb-connect-STM_options[families_$i]' value='0'".($this->options["families_$i"] == 0 ? " checked":"")."> No ";
        echo "<input type='radio' name='caleb-connect-STM_options[families_$i]' value=''".($this->options["families_$i"] === '' ? " checked":"")."> Possibly ";
        echo "<input type='radio' name='caleb-connect-STM_options[families_$i]' value='1'".($this->options["families_$i"] == 1 ? " checked":"")."> Must ";
    }

    public function groups_callback($args)
    {
        $i = $args['i'];
        echo "<input type='radio' name='caleb-connect-STM_options[groups_$i]' value='0'".($this->options["groups_$i"] == 0 ? " checked":"")."> No ";
        echo "<input type='radio' name='caleb-connect-STM_options[groups_$i]' value=''".($this->options["groups_$i"] === '' ? " checked":"")."> Possibly ";
        echo "<input type='radio' name='caleb-connect-STM_options[groups_$i]' value='1'".($this->options["groups_$i"] == 1 ? " checked":"")."> Must ";
    }

    public function married_callback($args)
    {
        $i = $args['i'];
        echo "<input type='radio' name='caleb-connect-STM_options[married_$i]' value='0'".($this->options["married_$i"] == 0 ? " checked":"")."> No ";
        echo "<input type='radio' name='caleb-connect-STM_options[married_$i]' value=''".($this->options["married_$i"] === '' ? " checked":"")."> Possibly ";
        echo "<input type='radio' name='caleb-connect-STM_options[married_$i]' value='1'".($this->options["married_$i"] == 1 ? " checked":"")."> Must ";
    }


    public function age_callback($args)
    {
        $i = $args['i'];
        printf(
            '<input type="text" id="age_'.$i.'" name="caleb-connect-STM_options[age_'.$i.']" value="%s" />',
            isset( $this->options["age_$i"] ) ? esc_attr( $this->options["age_$i"]) : ''
        );
    }


    public function pastDays_callback($args)
    {
        $i = $args['i'];
        printf(
            '<input type="text" id="pastDays_'.$i.'" name="caleb-connect-STM_options[pastDays_'.$i.']" value="%s" />',
            isset( $this->options["pastDays_$i"] ) ? esc_attr( $this->options["pastDays_$i"]) : ''
        );
    }

    public function category_callback($args)
    {
        $i = $args['i'];
        $categories = explode(',',$this->options['category_'.$i]);
        echo "<select id='category_$i' name='caleb-connect-STM_options[category_$i]'>";
        echo '<option value="">Any</option>';
        foreach ($args['caleb_tags']['categories'] as $key=>$value) {
           echo "<option value='$key'"; if (in_array($key,$categories)) echo " selected"; echo ">$value</option>>"; 
        }
        echo "</select>";
    }

    public function wp_category_callback($args)
    {
        $i = $args['i'];
        $category = $this->options['wp_category_'.$i];
        $wp_categories = get_terms('caleb_stm',array('hide_empty'=>0));
        echo "<select id='wp_category_$i' name='caleb-connect-STM_options[wp_category_$i]'>";
        echo '<option value="">None</option>';
        foreach ($wp_categories as $wp_category) {
           echo "<option value='$wp_category->term_id'"; if ($wp_category->term_id == $category) echo " selected"; echo ">$wp_category->name</option>>"; 
        }
        echo "</select>";
    }

    public function wp_tags_callback($args)
    {
        $i = $args['i'];
        echo "<input id='wp_category_$i' name='caleb-connect-STM_options[wp_tags_$i]' value=\"".$this->options["wp_tags_$i"]."\">";
    }

    public function wp_status_callback($args)
    {
        $i = $args['i'];
        echo "<select id='wp_status_$i' name='caleb-connect-STM_options[wp_status_$i]'>";
        foreach ($args['wp_statuses'] as $key=>$value) {
		   echo "<option value='$key'". ($key == $this->options['wp_status_'.$i] ? " selected":""). ">$value</option>>"; 
        }
        echo "</select>";
    }

    public function wp_comment_status_callback($args)
    {
        $i = $args['i'];
        echo "<select id='wp_comment_status_$i' name='caleb-connect-STM_options[wp_comment_status_$i]'>";
        foreach ($args['wp_comment_statuses'] as $key=>$value) {
		   echo "<option value='$key'". ($key == $this->options['wp_comment_status_'.$i] ? " selected":""). ">$value</option>>"; 
        }
        echo "</select>";
    }

    /**
    * Parses the URL field to determine if the cache was cleared and prints to screen if so.
    */
    public function parse_message()
    {
        if ( ! isset ( $_GET['cache'] ) && ! isset ( $_GET['STMs'] ) && !get_transient('caleb-connect-STM-error') 
            && ! isset ($_GET['stm_trash'])
            && ! isset ($_GET['STM_check'])
            )
            return;

        if ( isset ($_GET['imported'])) $imported = (int)$_GET['imported'];
        if ( isset ($_GET['updated'])) $updated = (int)$_GET['updated'];
        if ( isset ($_GET['trashed'])) $trashed = (int)$_GET['trashed'];

        if ( 'activated' === $_GET['cron'] )
            $this->text = 'Schedule activated';
        if ( 'deactivated' === $_GET['cron'] )
            $this->text = 'Schedule deactivated';
        else if ( 'done' === $_GET['STM_check'] ) 
            $this->text = 'Check done. '.$trashed.' STM(s) trashed';
        else if ( 'failed' === $_GET['STM_check'] ) {
            $this->errors[] = 'Something went wrong when checking STMs are still on Caleb';
            $error = get_transient('caleb-connect-STM-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-STM-error');
        }
        else if ( 'got' === $_GET['STMs'] ) {
            $this->text = $imported.' new STM(s) successfully converted to a post. '.$updated.' STM(s) updated';
            if ($trashed) $this->text .= '. '.$trashed.' job(s) trashed';
        }
        else if ( 'failed' === $_GET['STMs'] ) {
            $this->errors[] = 'Something went wrong when importing STMs as posts';
            $error = get_transient('caleb-connect-STM-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-STM-error');
        } else if ( 'done' === $_GET['stm_trash'] )
            $this->text = $trashed.' STM(s) trashed';
        else if ( 'failed' === $_GET['stm_trash'] ) {
            $this->errors[] = 'Something went wrong when trashing STMs';
            $error = get_transient('caleb-connect-STM-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-STM-error');
        } else if ($error = get_transient('caleb-connect-STM-error')) {
            $this->errors[] = 'Something went wrong automatically importing STMs as posts';
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-STM-error');
        }
        else return;

        if (isset($this->text)) {
            echo '<div class="updated"><p>'
                . $this->text . '</p></div>';
        }
        if (isset($this->errors))
            if (count($this->errors)) {
                echo "<div class='error'>";
                foreach ($this->errors as $error)
                    echo "<p>$error</p>";
                echo "</div>";
            }
        unset($this->errors);
    }

}

if( is_admin() ) {
    $om_settings_page = new CalebSTMsSettingsPage();
}

function get_STMs() {
    $STMGetter = new CalebSTMGetter();
    $url = urldecode( admin_url( 'edit.php?post_type=caleb_stm&page=caleb-connect-STMs') ) ;
    $result = $STMGetter->getNow();
    if ($result === false) 
        $url = add_query_arg( 'STMs', 'got', $url); 
    else {
        $url = add_query_arg( 'STMs', 'got', $url); 
        if (isset($STMGetter->number_trashed)) $url = add_query_arg( 'trashed', (int)$STMGetter->number_trashed, $url); 
        if (is_array($result)) {
            list($imported,$updated) = $result;
            $url = add_query_arg( 'imported',$imported,$url);
            $url = add_query_arg( 'updated',$updated,$url);
        }
    }
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_STMs_cron_activate() {
    if ( ! wp_next_scheduled('caleb_connect_STMs_cron') ) {
        wp_schedule_event( time(), 'daily', 'caleb_connect_STMs_cron');
    }
    $cron = 'activated';
    $url = add_query_arg( 'cron', $cron, urldecode( admin_url( 'edit.php?post_type=caleb_stm&page=caleb-connect-STMs') ) );
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_STMs_cron_deactivate() {
    wp_clear_scheduled_hook('caleb_connect_STMs_cron');
    $cron = 'deactivated';
    $url = add_query_arg( 'cron', $cron, urldecode( admin_url( 'edit.php?post_type=caleb_stm&page=caleb-connect-STMs') ) );
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_STMs_check_posts() {
    $getter = new CalebSTMGetter();
    $url = urldecode( admin_url( 'edit.php?post_type=caleb_stm&page=caleb-connect-STMs') ) ;
    $result = $getter->checkPosts();
    if ($result === false) 
        $url = add_query_arg( 'STM_check', 'failed', $url); 
    else {
        $url = add_query_arg( 'STM_check', 'done', $url); 
        $trashed = $result;
        $url = add_query_arg( 'trashed',(int)$trashed,$url);
    }
    wp_safe_redirect( $url );
    exit;
}

function trash_STMs() {
    $STMGetter = new CalebSTMGetter();
    $trashed = $STMGetter->trashAll();
    if ($trashed===false) { $trash = 'failed';}
    else $trash = 'done';
    $url = add_query_arg( 'stm_trash', $trash, urldecode( admin_url( 'edit.php?post_type=caleb_stm&page=caleb-connect-STMs') ) );
    $url = add_query_arg( 'trashed',(int)$trashed,$url);
    wp_safe_redirect( $url );
    exit;
}
?>
