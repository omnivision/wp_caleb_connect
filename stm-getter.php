<?php
/*
* file: getter.php - pulls the STMs from Caleb and saves them as posts.
* @author Chris Young <chris.young@om.org>
*/

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
class CalebSTMGetter {

    var $STMs;
    var $posts;
    var $number_successful_imports = 0;
    var $number_successful_updates = 0;
    var $number_trashed = 0;

    function __construct() {
        // Get options and put the feed options into a nested array for ease of use.
        $this->options = wp_parse_args(get_option('caleb-connect-STM_options'));
        for ($i=0; $i<$this->options['number_of_feeds']; $i++)
            foreach ($this->options as $k=>$v)
                if (strpos($k,(String)$i)==(strlen($k)-1))
                    $this->options['feeds'][$i][substr($k,0,-2)] = $v;
    }

    /*
    * function getNow() - get immediately rather than via CRON job
    * returns integer - number of affected rows.
    */
    function getNow()
    {
        $result = $this->get();
        if (is_wp_error($result)) {
            foreach ($result->errors as $error_group=>$errors) {
                foreach ($errors as $msg) {
                    set_transient('caleb-connect-STM-error',array(time(),$error_group,$msg));
                }
            }
            return false;
        }
        return array($this->number_successful_imports,$this->number_successful_updates);
    }

    /*
    * function runCron() - set up the CRON job
    * obeys "RSS Feed Cache Timeout" setting.
    */
    function runCron()
    {
        $result = $this->get();

        if (is_wp_error($result)) {
            foreach ($result->errors as $error_group=>$errors) {
                foreach ($errors as $msg) {
                    set_transient('caleb-connect-STM-error',array(time(),$error_group,$msg));
                }
            }
            return false;
        }
        else
        {
            set_transient('caleb-connect-STM-message',array(time(),'import',$this->number_successful_imports.' STMs imported as posts. '.$this->number_successful_updates.' STMs updated.'));
            return true;
        }
    }

    /*
    * function: get() - pull and save posts
    * returns integer - number of affected rows
    */
    function get()
    {
        $max_posts = 100;
        $result = $this->pull();
        if ($result) return $result;

        // Check to see if any posts exist that are not in our list
        $result = $this->checkPosts();
        if (is_wp_error($result)) return $result;
        else $this->number_trashed = $result;
        
        // Import the STMs in reverse date order - don't just do the first feed first!
        foreach ($this->STMs as $key => $row)
            $sort[$key] = strtotime($row->pubDate);
        array_multisort($sort,SORT_DESC,$this->STMs);

        foreach($this->STMs as $item) {
            $om = $item->children('om',true);
            $query = new WP_Query( 'post_type=caleb_stm&meta_key=_caleb_id&meta_value=G'.$om->id );
            if ($query->have_posts()) {
                $post = $query->posts[0];
                if (strpos($om->optionName,'CANCELLED')) { wp_trash_post($post->ID); continue; }
                if ($item->pubDate != get_post_meta($post->ID,'_caleb_pubDate',true)) {
                    $STM = $this->getSTM($om->id);
                    $STM['meta']['_caleb_pubDate'] = $item->pubDate;
                    $STM['post_id'] = $post->ID;
                    $this->posts[] = $STM;
                }
            } else {
                if (strpos($om->optionName,'CANCELLED')) continue;
                $STM = $this->getSTM($om->id);
                $STM['meta']['_caleb_pubDate'] = $item->pubDate;
                $STM['feed_id'] = $item['feed_id'];
                $this->posts[] = $STM;
            }
            if (count($this->posts) >= $max_posts) break;
        }
        $result = $this->save();
        return $result;
    }

    /*
    * function pull() - actually request the STMs from caleb
    * returns: array of STMs from caleb
    */
    function pull()
    {
        $options = $this->options; 

        $base_url = OM_APP_STMS_URL."?";

        for ($i=0;$i<$options['number_of_feeds'];$i++)
        {
            $feed_url = $base_url;
            if ($options["from_country"]) $feed_url .= '&fromCountryId='.$options["from_country"];
            if ($options["country_$i"]) $feed_url = $feed_url.'&country='.$options["country_$i"];
            if ($options["category_$i"]) $feed_url = $feed_url.'&category='.$options["category_$i"];
            if ($options["languages_$i"]) $feed_url = $feed_url.'&langCode='.$options["languages_$i"];
            if ($options["families_$i"]!='') $feed_url = $feed_url.'&families='.$options["families_$i"];
            if ($options["groups_$i"]!='') $feed_url = $feed_url.'&groups='.$options["groups_$i"];
            if ($options["married_$i"]!='') $feed_url = $feed_url.'&married='.$options["married_$i"];
            if ($options["age_$i"]) $feed_url = $feed_url.'&age='.$options["age_$i"];
            if ($options["pastDays_$i"]) $feed_url = $feed_url.'&pastDays='.$options["pastDays_$i"];
            $feed_url .= "&length=5000&plain=1";

            $url = html_entity_decode($feed_url);
            $response = wp_remote_get($url, array('timeout' => 15));
            if (!is_wp_error($response)) {
                $data = wp_remote_retrieve_body($response);
            }
            else return false;
            
            $data = simplexml_load_string($data);
            foreach($data->channel->item as $STM) {
                $STM['feed_id'] = $i;
                $this->STMs[] = $STM; 
            }
        }
    }

    /*
    * function: getSTM(String $STM_id)
    * Get a single STM from caleb using the ID string
    * Returns an array representing the STM
    */
    function getSTM($STM_id)
    {
        $url = OM_APP_STM_URL."?optionId=".$STM_id;
        $url .= '&fromCountryId='.$this->options['from_country'];
        $url = html_entity_decode($url);
        
        $response = wp_remote_get($url, array('timeout' => 15));
        if (!is_wp_error($response)) {
            $data = wp_remote_retrieve_body($response);
        }

        $data = simplexml_load_string($data);

        $item = $data->channel->item;
        $om = $item->children('om',true);
        $categories = str_replace(' | ',',',(String)$om->categories);
        $STM = array(
            'title'=>sanitize_text_field(html_entity_decode((String)$item->title,ENT_QUOTES)),
            'purpose'=>html_entity_decode((String)$om->purpose,ENT_QUOTES),
            'description'=>html_entity_decode((String)$om->description,ENT_QUOTES),
            'generalNotes'=>html_entity_decode((String)$om->generalNotes,ENT_QUOTES), // Added by sending field.
            'meta'=>array(
                'Country'=>(String)$om->country,
                'Event'=>(String)$om->eventName,
                'Start Date'=>(string)$om->startDate,
                'End Date'=>(string)$om->endDate,
                'Application Deadline'=>(string)$om->applicationDeadline,
                'Cost'=>(float)$om->cost, // Different for each sending field
                'Currency'=>(String)$om->currency, // Different for each sending field
                'Minimum Age'=>(int)$om->minAge,
                'Maximum Age'=>(int)$om->maxAge,
                'Ministry Details'=>html_entity_decode($om->ministryDetails,ENT_QUOTES),
                'Participant Profile'=>html_entity_decode((string)$om->participantProfile,ENT_QUOTES),
                'Accommodation'=>html_entity_decode((string)$om->accommodation,ENT_QUOTES),
                'Food'=>html_entity_decode((string)$om->food,ENT_QUOTES),
                'Travel'=>html_entity_decode((string)$om->travel,ENT_QUOTES),
                'Health'=>html_entity_decode((string)$om->health,ENT_QUOTES),
                'Visa'=>html_entity_decode((string)$om->visa,ENT_QUOTES),
                'Notes'=>html_entity_decode((string)$om->notes,ENT_QUOTES),
                '_married_welcome'=>(string)$om->marriedCouples,
                '_groups_welcome'=>(string)$om->groups,
                '_families_welcome'=>(string)$om->families,
                '_sending_country'=>(string)$om->sendingCountry,
                '_language'=>(string)$om->language,
                '_translations'=>(string)$om->translations,
                '_link'=>(String)$item->link,
                '_caleb_id'=>(String)$om->id,
                '_caleb_type'=>'STM',
                // There is no pubDate in the RSS. Truely.
                //'_caleb_pubDate'=>(String)$item->pubDate,
                ),
            'categories'=>$categories
            );
        return $STM;
    }

    /*
    * function save() - save new STMs as posts in wordpress
    * returns integer - number of rows inserted
    */
    function save()
    {
        $number_successful_imports = 0;
        $number_successful_updates = 0;
        
        if (!is_array($this->posts)) return true;

        foreach ($this->posts as $item)
        {
            $i = (Int)$item['feed_id'];
            if ($item['post_id']) {
                $post = $item;
                $post['ID'] = $item['post_id'];
                $new = false;
            } else {
                $new = true;
                $post = array(
                    'post_type'     => 'caleb_stm',
                    'post_name'     => sanitize_title($item['title'].'-'.(String)$item['meta']['_caleb_id']),
                    'post_category' => array($this->options['feeds'][$i]['wp_category']),
                    'tags_input'    => $item['categories'].','.$this->options['feeds'][$i]['wp_tags'], 
                    'post_status'   => $this->options['feeds'][$i]['wp_status'], 
                    'comment_status'=> $this->options['feeds'][$i]['wp_comment_status'],
                    'post_title'    => $item['title'],
                    'post_date'     => date('Y-m-d H:i:s',strtotime($item['meta']['_caleb_pubDate'])),
                );
                $post['ID'] = wp_insert_post($post,true);
                if (is_wp_error($post['ID'])) return $post['ID'];
            }

            $meta = "";
            foreach ($item['meta'] as $key=>$val)
                if ($val && substr($key,0,1)!='_') $meta .= "<b>$key:</b> $val\n";
            $meta = substr($meta,0,-1);
             
            foreach ($item['meta'] as $key=>$val)
                update_post_meta($post['ID'],$key,(String)$val);
            
            $content = str_replace(array('<p>','</p>'),"\n",html_entity_decode($item['purpose']."\n\n".$item['description']."\n\n".$item['generalNotes'],ENT_QUOTES));
            
            $post = array(
                'ID'            => $post['ID'],
                'post_content'  => "<div class=\"om_STM_description\">".$content."</div>\n"
                                  ."<div class=\"om_STM_meta\">$meta</div>",
                'post_excerpt'  => html_entity_decode($item['description'],ENT_QUOTES), 
                'post_date'     => date('Y-m-d H:i:s',strtotime($item['meta']['_caleb_pubDate'])),
                'edit_date'     => true,
                'post_title'    => sanitize_text_field(html_entity_decode($item['title'],ENT_QUOTES)),
                );

            $result = wp_update_post($post,true);
            if (is_wp_error($result)) return $result; // pass back wordpress error
            else if ($new) $number_successful_imports++;
            else $number_successful_updates++;

        }
        $this->number_successful_imports = $number_successful_imports;
        $this->number_successful_updates = $number_successful_updates;
        return true;
    }

    /* function checkposts()
    * check for posts that have been removed from caleb.
    */
    function checkposts()
    {
        $passed = $faled = 0;
        if (!count($this->STMs)) $this->pull();
        foreach ($this->STMs as $STM)
        {
            $om = $STM->children('om',true);
            $ids[] = 'G'.(string)$om->id;
        }
        $query = new wp_query(array(
                    'posts_per_page'=>'-1',
                    'post_type'=>'caleb_stm',
                    'meta_query' => array(
                            array(
                                'key' => '_caleb_id',
                                'value' => $ids,
                                'compare'=> 'not in'
                                )
                    )
        ));
        $query->get_posts();
        while ($query->have_posts())
        {
            $query->next_post();
            wp_trash_post($query->post);
            $failed++;
        }
        $this->number_trashed = $failed;
        return $failed;

    }

    /**
    * trashAll() 
    * trash all imported STMs from caleb
    **/
    function trashAll() {
        $i = 0;
        $query = new WP_Query("post_type=caleb_stm&posts_per_page=-1");
        while ($query->have_posts()) {
            $query->next_post();
            if ($result = wp_trash_post($query->post->ID)) $i++;
            else if (is_wp_error($result)) die(print_r($result)); //return false;
        }
        return $i;
    }
}

function caleb_connect_STMs_cron() {
    $STMGetter = new CalebSTMGetter();
    $STMGetter->runCron();
}

add_action('init','create_STM_group_taxonomy');
add_action('init','create_STM_tag_taxonomy');
function create_STM_group_taxonomy() {
    register_taxonomy(
        'stm_group',
        'caleb_stm',
        array(
            'hierarchical'=>true,
            'show_admin_column'=>true,
            'labels'=> array(
                'name'=> __('Groups'),
                'menu_name'=> __('STM Groups'),
                'singular_name'=> __('Group')
            ),
            'rewrite'=>array('slug'=>__('short-term-missions-by-group')),
        )
    );
}
function create_STM_tag_taxonomy() {
    register_taxonomy(
        'stm_tags',
        'caleb_stm',
        array(
            'hierarchical'=>false,
            'show_admin_column'=>true,
            'labels'=> array(
                'name'=> __('Tags'),
                'menu_name'=> __('STM Tags'),
            ),
            'rewrite'=>array('slug'=>__('short-term_missions-by-tag')),
        )
    );
}
add_action('init','create_STM_post_type');
function create_STM_post_type() {
    register_post_type('caleb_stm',
        array(
            'label'=>'Short Term trips',
            'public'=>true,
            'has_archive'=>true,
            'supports'=>array(
                'title','editor','excerpt','custom-fields','revisions'
            ),
            'menu_icon'=>'dashicons-migrate',
            'rewrite'=>array('slug'=>__('short-term-mission-trips')),
        )
    );
    register_taxonomy_for_object_type('stm_group','caleb_stm');
    register_taxonomy_for_object_type('stm_tags','caleb_stm');
}
?>
