<?php  

if (!function_exists('safe_array_get')) {
    function safe_array_get(&$arr, $key, $default=null)
    {
        return (isset($arr[$key]) ? $arr[$key] : $default);
    }

}

class CalebConnectResourceSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    public $feeds;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_posts_page(
            'Caleb Connect :: Caleb Resources', 
            'Caleb Connect', 
            'manage_options', 
            'caleb-connect-resources', 
            array( $this, 'create_admin_page' )
        );

        // display admin notice when cache has been cleared
        add_action( "admin_notices", array ( $this, 'parse_message' ) );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'caleb-connect-resource_options' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Caleb Connect :: Caleb Resources</h2>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'caleb_connect_resource_option_group' );   
                do_settings_sections( 'caleb-connect-resources' );

                submit_button("Update Settings"); 
            ?>
            </form>

            <h3>Scheduled Imports</h3>
            Currently, daily imports are <?php echo ( wp_next_scheduled('caleb_connect_resources_cron') ? 'indeed' : 'not' ); ?> scheduled.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                 <?php if (wp_next_scheduled('caleb_connect_resources_cron')) : ?> 
                <input type="hidden" name="action" value="caleb_connect_resources_cron_deactivate" />
                <input type="submit" name="button_deactivate" value="Deactivate" class="button" />
                <?php else : ?>
                <input type="hidden" name="action" value="caleb_connect_resources_cron_activate" />
                <input type="submit" name="button_activate" value="Activate" class="button" />
                <?php endif; ?>
            </form>
            </p>
            <h3>Do it now</h3>
            Download the resources now, rather than waiting.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="get_now" />
                <input type="submit" name="button_get_resources" value="Download" class="button" />
            </form>
            </p>
            <h3>Check Posts</h3>
            Check for posts that are no longer on caleb.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="check_caleb_resources" />
                <input type="submit" name="button_check_posts" value="Check" class="button" />
            </form>
            </p>
            <h3>Delete Resources</h3>
            Delete all the imported resources now.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="delete_now" />
                <input type="submit" name="button_delete_resources" value="Delete" class="button" />
            </form>
            </p>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        if (!is_array($this->feeds)) {
            $this->feeds = array();
        }
        $this->feeds[] = array("type"=>4,"countryId"=>"AU");

        include('resource-options.php');

        register_setting(
            'caleb_connect_resource_option_group', // Option group
            'caleb-connect-resource_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'caleb-connect_general', // ID
            'Resource Downloader', // Title
            array( $this, 'general_section_info' ), // Callback
            'caleb-connect-resources' // Page
        );  
        
        add_settings_field(
            'number_of_feeds', // ID
            'Number of Feeds', // Title 
            array( $this, 'number_of_feeds_callback' ), // Callback
            'caleb-connect-resources', // Page
            'caleb-connect_general' // Section           
        );
        $this->options = get_option( 'caleb-connect-resource_options' );
        for ($i=0;$i<$this->options['number_of_feeds'];$i++) {

            add_settings_section(
                'caleb-connect_feed'.$i, // ID
                'Feed '.($i+1), // Title
                array( $this, 'feed_section_info' ), // Callback
                'caleb-connect-resources' // Page
            );  
            
            add_settings_field(
                'resource_type', // ID
                'Resource Type', // Title 
                array($this,'resource_type_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'country', // ID
                'Country', // Title 
                array($this, 'country_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i, 'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'category', // ID
                'Caleb Category', // Title 
                array($this,'category_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'language', // ID
                'Language Code', // Title 
                array($this,'language_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'from_date', // ID
                'Earliest Resource Date', // Title 
                array($this,'from_date_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );

            add_settings_field(
                'discard_articles_with_no_image', // ID
                'Discard articles with no image', // Title 
                array($this,'discard_articles_with_no_image_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_category', // ID
                'Wordpress Category', // Title 
                array($this,'wp_category_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_tags', // ID
                'Wordpress Tags (comma separated)', // Title 
                array($this,'wp_tags_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_status', // ID
                'Wordpress Status', // Title 
                array($this,'wp_status_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'wp_statuses'=>$wp_statuses) // Args for callback           
            );
            add_settings_field(
                'wp_comment_status', // ID
                'Wordpress Comment Status', // Title 
                array($this,'wp_comment_status_callback'), // Callback
                'caleb-connect-resources', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'wp_comment_statuses'=>$wp_comment_statuses) // Args for callback           
            );
        }

        add_action('admin_post_get_now', 'get_resources');
        add_action('admin_post_delete_now', 'delete_resources');
        add_action('admin_post_check_caleb_resources', 'caleb_connect_resources_check_posts');
        add_action('admin_post_caleb_connect_resources_cron_activate', 'caleb_connect_resources_cron_activate');
        add_action('admin_post_caleb_connect_resources_cron_deactivate', 'caleb_connect_resources_cron_deactivate');
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['pin'] ) && $input['pin'] != '' )
            $new_input['pin'] = sanitize_text_field($input['pin']);

        
        if( isset( $input['field_id'] ) )
            $new_input['field_id'] = sanitize_text_field( $input['field_id'] );

        if (isset($input['number_of_feeds'])) {
            // validate data and always be at least 1 feed.  
            $input['number_of_feeds'] = absint($input['number_of_feeds']);
            $new_input['number_of_feeds'] = max($input['number_of_feeds'], 1);
        }

        for ($i=0; $i<$this->options['number_of_feeds']; $i++) {
            if( isset( $input['resource_type_'.$i] ) )
                $new_input['resource_type_'.$i] = $this::sanitize_multiselect($input['resource_type_'.$i]);
            if( isset( $input['country_'.$i] ) )
                $new_input['country_'.$i] = $this::sanitize_multiselect($input['country_'.$i],'String');
            if( isset( $input['category_'.$i] ) )
                $new_input['category_'.$i] = $this::sanitize_multiselect($input['category_'.$i]);
            if( isset( $input['language_'.$i] ) )
                $new_input['language_'.$i] = strtolower($input['language_'.$i]);
            if( isset( $input['discard_articles_with_no_image_'.$i] ) )
                $new_input['discard_articles_with_no_image_'.$i] = (int)$input['discard_articles_with_no_image_'.$i];
            if( isset( $input['wp_category_'.$i] ) )
                $new_input['wp_category_'.$i] = $input['wp_category_'.$i];
            if( isset( $input['wp_tags_'.$i] ) )
                $new_input['wp_tags_'.$i] = $input['wp_tags_'.$i];
            if( isset( $input['wp_status_'.$i] ) )
                $new_input['wp_status_'.$i] = $input['wp_status_'.$i];
            if( isset( $input['wp_comment_status_'.$i] ) )
                $new_input['wp_comment_status_'.$i] = $input['wp_comment_status_'.$i];

            // turn 3 date fields into single yyyy-mm-dd string:
            if( isset( $input['from_year_'.$i] ) )
                $from_date = (int)$input['from_year_'.$i];
            if( isset( $input['from_month_'.$i] ) )
                $from_date .= '-'. (int)$input['from_month_'.$i];
            if( isset( $input['from_day_'.$i] ) )
                $from_date .= '-'. (int)$input['from_day_'.$i];

            // check that year-month-day is a valid date:
            if (strtotime($from_date))
                $new_input['from_date_'.$i] = strtotime($from_date);

        }

        return $new_input;
    }

    // Make sure an array is used, cast the values if needed, and serialize for storage.
    private static function sanitize_multiselect($input,$cast = "Int")
    {
        $input_array = (Array)$input;
        $output = array();

        foreach ($input_array as $k=>$v) {
            if ($v=='') continue;
            switch ($cast) {
                case "Int": $output[$k] = (Int)$v;
                case "String": $output[$k] = (String)$v;
                default: $output[$k] = $v;
            }
        }
        return serialize($output);
    }

    /** 
     * Print the Section header text
     */
    public function general_section_info()
    {
        global $sitepress;
        if ($sitepress && !function_exists('wpml_get_content_translation')) {
            if(!defined('WPML_LOAD_API_SUPPORT')) {
                print('<div class="error">WARNING! WPML (sitepress) exists, but WPML_LOAD_API_SUPPORT is not set in your wp-config.php!</div>');
            } else {
                print('<div class="error">WARNING! WPML (sitepress) exists, WPML_LOAD_API_SUPPORT is defined, but somehow the functions I need to do translations do NOT exist!</div>');
            }
        }
        print 'Choose how many different feeds of resources you would like, then choose options for each feed';
    }

    /** 
     * Print the Section header text
     */
    public function feed_section_info()
    {
        print 'Choose what resources you want and where you want them to go.';
    }

    public function pin_callback()
    {
        printf(
            '<input type="text" id="pin" name="caleb-connect-resource_options[pin]" value="%s" />',
            isset( $this->options['pin'] ) ? esc_attr( $this->options['pin']) : ''
        );
    }
    
    public function field_id_callback()
    {
        printf(
            '<input type="text" id="Field ID" name="caleb-connect-resource_options[field_id]" value="%s" />',
            isset( $this->options['field_id'] ) ? esc_attr( $this->options['field_id']) : ''
        );
    }

    public function number_of_feeds_callback()
    {
        printf(
            '<input type="number" id="number_of_feeds" name="caleb-connect-resource_options[number_of_feeds]" value="%s" />',
            isset( $this->options['number_of_feeds'] ) ? esc_attr( $this->options['number_of_feeds']) : ''
        );
        echo '<button type="submit">Update</button>';
    }

    public function resource_type_callback($args)
    {
        $i = $args['i'];
        $types = (Array)unserialize($this->options['resource_type_'.$i]);
        echo "<select id='resource_type_$i' name='caleb-connect-resource_options[resource_type_$i][]' multiple='multiple'>";
        foreach ($args['caleb_tags']['resource_types'] as $key=>$value) {
           echo "<option value='$key'"; if (in_array($key,$types)) echo " selected"; echo ">$value</option>";
        }
        echo "</select>";
    }

    public function discard_articles_with_no_image_callback($args)
    {
        $i = $args['i'];
        echo "<input type='checkbox' id='discard_articles_with_no_image_$i' name='caleb-connect-resource_options[discard_articles_with_no_image_$i]' value='1'".($this->options["discard_articles_with_no_image_$i"]?' checked':'').">";
    }

    public function country_callback($args)
    {
        $i = $args['i'];
        if (isset($this->options['country_' . $i])) {
            $countries = (Array)unserialize($this->options['country_'.$i]);
        } else {
            $countries = array();
        }

        for($c=0;$c<count($countries);$c++){
            if ($countries[$c] === '') {
                unset($countries[$c]);
            }
        }

        echo "<select id='country_$i' name='caleb-connect-resource_options[country_$i][]' multiple='multiple'>";
        echo "<option value=''", (count($countries)< 1 ? " selected" : ""), ">Any</option>";
        foreach ($args['caleb_tags']['countries'] as $key=>$value) {
               echo "<option value='$key'",
                            ((count($countries) && in_array($key,$countries))?" selected":""),
                            ">$value</option>"; 
        }
        echo "</select>";
    }

    public function category_callback($args)
    {
        $i = $args['i'];
        if (isset($this->options["category_$i"])) {
            $categories = (Array)unserialize($this->options['category_'.$i]);
        } else {
            $categories = array();
        }
   
        for($c=0;$c<count($categories);$c++){
            if ($categories[$c] === '') {
                unset($categories[$c]);
            }
        }

        echo "<select id='category_$i' name='caleb-connect-resource_options[category_$i][]' multiple='multiple'>";
        echo '<option ', (!count($categories)?'selected ':''), 'value="">Any</option>';
        foreach ($args['caleb_tags']['categories'] as $key=>$value) {
           echo "<option value='$key'", (in_array($key,$categories)? " selected": ""), ">$value</option>>"; 
        }
        echo "</select>";
    }

    public function language_callback($args)
    {
        $i = $args['i'];
        echo "<input id='language_$i' name='caleb-connect-resource_options[language_$i]' value=\"".$this->options["language_$i"]."\">";
    }

    public function from_date_callback($args)
    {

        $i = $args['i'];
        if (isset($this->options["from_date_$i"])) {
            $from_date = (int)$this->options["from_date_$i"];
            if ($from_date < strtotime('1966-01-01')) {
                $from_date = strtotime('1966-01-01');
            }
            if ($from_date > time()) {
                echo "<div class='error'>", __("Date in the future!"), '</div>';
            }
        } else {
            $from_date = time()-(60*60*24*365);
        }

        $year = date('Y',$from_date);
        $month = date('m',$from_date);
        $day = date('d',$from_date);
        echo "<input id='from_day_$i' name='caleb-connect-resource_options[from_day_$i]' value=\"".$day."\" size=\"2\" maxlength=\"2\"> / ";
        echo "<input id='from_month_$i' name='caleb-connect-resource_options[from_month_$i]' value=\"".$month."\" size=\"2\" maxlength=\"2\"> / ";
        echo "<input id='from_year_$i' name='caleb-connect-resource_options[from_year_$i]' value=\"".$year."\" size=\"4\" maxlength=\"4\">";
    }

    public function wp_category_callback($args)
    {
        $i = $args['i'];
        $category = $this->options['wp_category_'.$i];
        $wp_categories = get_categories(array('hide_empty'=>0));
        echo "<select id='wp_category_$i' name='caleb-connect-resource_options[wp_category_$i]'>";
        foreach ($wp_categories as $wp_category) {
           echo "<option value='$wp_category->term_id'"; if ($wp_category->term_id == $category) echo " selected"; echo ">$wp_category->name</option>>"; 
        }
        echo "</select>";
    }

    public function wp_tags_callback($args)
    {
        $i = $args['i'];
        echo "<input id='wp_category_$i' name='caleb-connect-resource_options[wp_tags_$i]' value=\"".$this->options["wp_tags_$i"]."\">";
    }

    public function wp_status_callback($args)
    {
        $i = $args['i'];
        echo "<select id='wp_status_$i' name='caleb-connect-resource_options[wp_status_$i]'>";
        foreach ($args['wp_statuses'] as $key=>$value) {
           echo "<option value='$key'". ($key == $this->options['wp_status_'.$i] ? " selected":""). ">$value</option>>"; 
        }
        echo "</select>";
    }

    public function wp_comment_status_callback($args)
    {
        $i = $args['i'];
        echo "<select id='wp_comment_status_$i' name='caleb-connect-resource_options[wp_comment_status_$i]'>";
        foreach ($args['wp_comment_statuses'] as $key=>$value) {
           echo "<option value='$key'". ($key == $this->options['wp_comment_status_'.$i] ? " selected":""). ">$value</option>>"; 
        }
        echo "</select>";
    }

    /**
    * Parses the URL field to determine if the cache was cleared and prints to screen if so.
    */
    public function parse_message()
    {
        if ( ! isset ( $_GET['cache'] ) && ! isset ( $_GET['resources'] ) && ! isset ($_GET['delete']) 
            && ! isset ($_GET['resource_check'])
            )
            return;

        if ( isset ($_GET['inserted'])) $inserted = (int)$_GET['inserted'];
        if ( isset ($_GET['updated'])) $updated = (int)$_GET['updated'];
        if ( isset ($_GET['discarded'])) $discarded = (int)$_GET['discarded'];
        if ( isset ($_GET['deleted'])) $deleted = (int)$_GET['deleted'];
        if ( isset ($_GET['urls'])) $urls = $_GET['urls'];

        //if ( 'activated' === $_GET['cron'] )
        //    $this->text = 'Schedule activated';
        $do_cron = safe_array_get($_GET, 'cron');
        $do_resource_check = safe_array_get($_GET, 'resource_check');
        $do_resources = safe_array_get($_GET, 'resources');
        $do_delete = safe_array_get($_GET, 'delete');

        if ('activated' === $do_cron) {
            $this->text = 'Schedule activated';
        }

        if ('deactivated' === $do_cron) {
            $this->text = 'Schedule deactivated';
        } elseif ('done' === $do_resource_check) {
            $this->text = 'Check done. '.$deleted.' resource(s) trashed';
        } elseif ('failed' === $do_resource_check) {
            $this->errors[] = 'Something went wrong when checking resources are still on Caleb';
            $error = get_transient('caleb-connect-resource-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-resource-error');
        }
        else if ( 'got' === $do_resources ) {
            $this->text = $inserted.' new resource(s) successfully converted to a post. ';
            if ($updated) $this->text .= $updated.' post(s) successfully updated.';
            if ($discarded) $this->text .= $discarded.' post(s) discarded.';
            if ($deleted) $this->text .= $deleted.' post(s) trashed.';
            if ($urls) $this->text .= '{'. $urls.'}'; 
        } else if ( 'failed' === $do_resources ) {
            $this->errors[] = 'Something went wrong when importing resources as posts';
            $error = get_transient('caleb-connect-resource-error');
            if ($error[0]) $this->errors[] = date('d/m/Y H:i',$error[0]).' - '.$error[1].": ".$error[2];
            delete_transient('caleb-connect-resource-error');
        }
        else if ( 'done' === $do_delete ) {
            $this->text = $deleted.' resource(s) deleted. ';
        } else if ( 'failed' === $do_resources) {
            $this->errors[] = 'Something went wrong when deleting';
        }
        else return;
        
        if (isset($this->text)) {
            echo '<div class="updated"><p>'
                . $this->text . '</p></div>';
        }
        if (isset($this->errors))
            if (count($this->errors)) {
                echo "<div class='error'>";
                foreach ($this->errors as $error)
                    echo "<p>$error</p>";
                echo "</div>";
            }
        unset($this->error);
    }

}

if( is_admin() ) {
    $om_settings_page = new CalebConnectResourceSettingsPage();
}

function get_resources() {
    $getter = new CalebResourceGetter();
    $result = $getter->getNow();
    if ($result===false) { $resources = 'failed';}
    else $resources = 'got';
    $stats = $getter->getStats();
    $url = add_query_arg( 'resources', $resources, urldecode( admin_url( 'edit.php?page=caleb-connect-resources') ) );
    $url = add_query_arg( 'inserted',$stats['num_imports'],$url);
    $url = add_query_arg( 'updated',$stats['num_updates'],$url);
    $url = add_query_arg( 'discarded',$stats['num_discarded'],$url);
    $url = add_query_arg( 'deleted',$stats['num_deleted'],$url);
    $url = add_query_arg( 'urls',$stats['urls'],$url);
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_resources_cron_activate() {
    if ( ! wp_next_scheduled('caleb_connect_resources_cron') ) {
        wp_schedule_event( time(), 'daily', 'caleb_connect_resources_cron');
    }
    $cron = 'activated';
    $url = add_query_arg( 'cron', $cron, urldecode( admin_url( 'edit.php?page=caleb-connect-resources') ) );
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_resources_cron_deactivate() {
    wp_clear_scheduled_hook('caleb_connect_resources_cron');
    $cron = 'deactivated';
    $url = add_query_arg( 'cron', $cron, urldecode( admin_url( 'edit.php?page=caleb-connect-resources') ) );
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_resources_check_posts() {
    $getter = new CalebResourceGetter();
    $url = urldecode( admin_url( 'edit.php?page=caleb-connect-resources') ) ;
    $result = $getter->checkPosts();
    if ($result === false) 
        $url = add_query_arg( 'resource_check', 'failed', $url); 
    else {
        $url = add_query_arg( 'resource_check', 'done', $url); 
        $trashed = $result;
        $url = add_query_arg( 'deleted',(int)$trashed,$url);
    }
    wp_safe_redirect( $url );
    exit;
}

function delete_resources() {
    $getter = new CalebResourceGetter();
    $deleted = $getter->deleteAll();
    if ($deleted===false) { $delete = 'failed';}
    else $delete = 'done';
    $stats = $getter->getStats();
    $url = add_query_arg( 'delete', $delete, urldecode( admin_url( 'edit.php?page=caleb-connect-resources') ) );
    $url = add_query_arg( 'deleted',(int)$stats['num_deleted'],$url);
    wp_safe_redirect( $url );
    exit;
}

?>
