<?php

add_action( 'widgets_init', 'register_caleb_jobs_widget' );

function register_caleb_jobs_widget() {
	register_widget( 'Caleb_Jobs_Widget' );
}

class CalebJobsCollapsable {
    /* In a widget, if you want a section to be collapsable, use this.
        Collapsable::start('Links');
           echo 'stuff inside the widget...';
        Collapsable::end();
    */
    public function start($title, $id) {
        echo '<div class="widget ' . $id . '"><div class="widget-top" style="cursor:pointer"><div class="widget-title-action"><a class="widget-action hide-if-no-js" href="#"></a></div><div class="widget-title"><h5>', __($title, 'caleb_jobs_widget'), '</h5></div></div><div class="widget-inside">';
    }
    public function end() {
        echo '</div></div>';
    }

}


class Caleb_Jobs_Widget extends WP_Widget {

	function Caleb_Jobs_Widget() {
		$widget_ops = array( 'classname' => 'caleb_jobs_widget',
		                   'description' => __('A simple widget for showing jobs as a list.', 'caleb_jobs_widget'));
		$control_ops = array('height' => 300,
		                     'id_base' => 'caleb_jobs_widget',
                             'categories'=>array());

		$this->WP_Widget( 'caleb_jobs_widget', __('Caleb Jobs Widget', 'caleb_jobs_widget'), $widget_ops, $control_ops);

	}

	function widget( $args, $instance ) {
		extract ($args);
		$show_info = isset( $instance['show_info'] ) ? $instance['show_info'] : false;

		echo $before_widget;

		?>
			<div class="caleb_jobs_widget-box group" style="width:80%;margin:auto;">
        <?php
                $query = new WP_Query(
                    array(
                        'tax_query'=>array(
                            array(
                                'taxonomy'=>'job_group',
                                'terms'=>$instance['categories'],
                                'operator'=>'IN',
                            ),
                        ),
                        'post_type'=>'caleb_job',
                        'posts_per_page' =>3
                        
                    )
                );
                while ($query->have_posts()) {
                $query->the_post();

                ?>
                <div class=""><article class="caleb_jobs_widget_post caleb_job">
                    <header class="entry-header">
                        <a href="<?php echo get_permalink(); ?>">
                          <h1 class="entry-title"><?php the_title(); ?></h1>
                          <?php the_post_thumbnail(); ?>
                          </a>
                    </header>
                    <div class="entry-summary">
                          <?php the_excerpt();?>
                    <a class="more-link" href="<?php echo get_permalink(); ?>"><?php _e("Read More") ?></a>
                    </div>
                </article>
                </div>
                <?php }

                 wp_reset_postdata();
             ?>
        </div>
		<?php // scrollbox

		echo $after_widget;

	}

	function update ( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['categories'] = (array) $new_instance['categories'];
        
		return $instance;
	}

	function form ( $instance ) {
		$defaults =  array('categories'=>array());

		$instance = wp_parse_args( (array) $instance, $defaults);

        // Page Links Options:

        $pages = array();

        CalebJobsCollapsable::start('Limit jobs to categories...', 'post_categories');
            foreach (get_terms('job_group') as $category) {
                echo '<input type="checkbox" id="' . $this->get_field_id('categories') .'[]"
                      name="' . $this->get_field_name( 'categories' ) . '[]" ' .
                      checked(in_array($category->term_id,$instance['categories']), true, false) .
                      ' value="' . $category->term_id . '" /><label>'. $category->name .'</label><br/>'; 
            }
            
            if (!$categories) {
                echo "No categories are defined.";
            }

        CalebJobsCollapsable::end();

        echo '<hr/>';

	}

	function form_control($name, $label, $instance, $type) {
		$id = $this->get_field_id($name);
		$value = ($type=='checkbox')? 
		             checked(true, isset($instance[$name])?$instance[$name]:false, false):
		             'value="'. $instance[$name] .'"';
		printf('<p><label for="%s">%s </label> <input id="%s" name="%s" %s type="%s"/></p>',
		       $id, __($label,'caleb_jobs_widget'), $id,
		       $this->get_field_name($name), $value, $type);
	}

    function form_select($name, $label, $instance, $options) {
        echo '<label>' . __($label) . ' <select name="'. $this->get_field_name($name) . '">';
            foreach ($options as $id => $title) {
                echo '<option value="'. $id . '" '
                     . ( $instance[$name] == $id ? "selected" : "" )
                     .'>' . $title . '</option>';
            }
            echo '</select></label><br/>';

    }


}

?>
