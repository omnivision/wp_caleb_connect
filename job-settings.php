<?php  
class CalebJobsSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_submenu_page(
            'edit.php?post_type=caleb_job',
            'Caleb Connect :: Caleb Jobs', 
            'Caleb Connect', 
            'manage_options', 
            'caleb-connect-jobs', 
            array( $this, 'create_admin_page' )
        );

        // display admin notices
        add_action( "admin_notices", array ( $this, 'parse_message' ) );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'caleb-connect-job_options' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Caleb Connect :: Caleb Jobs</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'caleb_connect_jobs_option_group' );   
                do_settings_sections( 'caleb-connect-jobs' );

                submit_button("Update Settings"); 
            ?>
            </form>

            <h3>Scheduled Job Imports</h3>
            Currently, daily job imports are <?php echo ( wp_next_scheduled('caleb_connect_jobs_cron') ? 'indeed' : 'not' ); ?> scheduled.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                 <?php if (wp_next_scheduled('caleb_connect_jobs_cron')) : ?> 
                <input type="hidden" name="action" value="caleb_connect_jobs_cron_deactivate" />
                <input type="submit" name="button_deactivate" value="Deactivate" class="button" />
                <?php else : ?>
                <input type="hidden" name="action" value="caleb_connect_jobs_cron_activate" />
                <input type="submit" name="button_activate" value="Activate" class="button" />
                <?php endif; ?>
            </form>
            </p>
            <h3>Do it now</h3>
            Download the jobs now, rather than waiting.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="get_jobs" />
                <input type="submit" name="button_get_jobs" value="Download" class="button" />
            </form>
            </p>
            <h3>Check Posts</h3>
            Check for posts that are no longer on caleb.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="check_posts" />
                <input type="submit" name="button_check_posts" value="Check" class="button" />
            </form>
            </p>
            <h3>Trash Jobs</h3>
            Trash all the imported jobs now.
            <p>
            <form action="admin-post.php" method="post">
                 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
                <input type="hidden" name="action" value="trash_now" />
                <input type="submit" name="button_trash_jobs" value="Trash" class="button" />
            </form>
            </p>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        include('job-options.php');

        register_setting(
            'caleb_connect_jobs_option_group', // Option group
            'caleb-connect-job_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'caleb-connect_general', // ID
            'General Settings', // Title
            array( $this, 'general_section_info' ), // Callback
            'caleb-connect-jobs' // Page
        );  
        
        add_settings_field(
            'number_of_feeds', // ID
            'Number of Feeds', // Title 
            array( $this, 'number_of_feeds_callback' ), // Callback
            'caleb-connect-jobs', // Page
            'caleb-connect_general' // Section           
        );
        $this->options = get_option( 'caleb-connect-job_options' );
        for ($i=0;$i<$this->options['number_of_feeds'];$i++) {

            add_settings_section(
                'caleb-connect_feed'.$i, // ID
                'Feed '.($i+1), // Title
                array( $this, 'feed_section_info' ), // Callback
                'caleb-connect-jobs' // Page
            );  
            
            add_settings_field(
                'country', // ID
                'Country', // Title 
                array($this,'country_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'category', // ID
                'Caleb Category', // Title 
                array($this,'category_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'region', // ID
                'Region/Religion', // Title 
                array($this,'region_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'caleb_tags'=>$caleb_tags) // Args for callback           
            );
            add_settings_field(
                'wp_category', // ID
                'Wordpress Category', // Title 
                array($this,'wp_category_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_tags', // ID
                'Wordpress Tags (comma separated)', // Title 
                array($this,'wp_tags_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i) // Args for callback           
            );
            add_settings_field(
                'wp_status', // ID
                'Wordpress Status', // Title 
                array($this,'wp_status_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'wp_statuses'=>$wp_statuses) // Args for callback           
            );
            add_settings_field(
                'wp_comment_status', // ID
                'Wordpress Comment Status', // Title 
                array($this,'wp_comment_status_callback'), // Callback
                'caleb-connect-jobs', // Page
                'caleb-connect_feed'.$i, // Section
                array('i'=>$i,'wp_comment_statuses'=>$wp_comment_statuses) // Args for callback           
            );
        }

        add_action('admin_post_get_jobs', 'get_jobs');
        add_action('admin_post_check_posts', 'caleb_connect_jobs_check_posts');
        add_action('admin_post_trash_now', 'trash_jobs');
        add_action('admin_post_caleb_connect_jobs_cron_activate', 'caleb_connect_jobs_cron_activate');
        add_action('admin_post_caleb_connect_jobs_cron_deactivate', 'caleb_connect_jobs_cron_deactivate');
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        
        if( isset( $input['number_of_feeds'] ) )
            // validate data and always be at least 1 feed.  
            $input['number_of_feeds'] = absint($input['number_of_feeds']);
            $new_input['number_of_feeds'] = ($input['number_of_feeds'] > 0 ? $input['number_of_feeds'] : 1);

        for ($i=0;$i<$this->options['number_of_feeds'];$i++)
        {
            if( isset( $input['country_'.$i] ) )
                $new_input['country_'.$i] = $this::sanitize_multiselect($input['country_'.$i],'String');
            if( isset( $input['category_'.$i] ) )
                $new_input['category_'.$i] = $this::sanitize_multiselect($input['category_'.$i]);
            if( isset( $input['region_'.$i] ) )
                $new_input['region_'.$i] = $this::sanitize_multiselect($input['region_'.$i]);
            if( isset( $input['wp_category_'.$i] ) )
                $new_input['wp_category_'.$i] = $input['wp_category_'.$i];
            if( isset( $input['wp_tags_'.$i] ) )
                $new_input['wp_tags_'.$i] = $input['wp_tags_'.$i];
            if( isset( $input['wp_status_'.$i] ) )
                $new_input['wp_status_'.$i] = $input['wp_status_'.$i];
            if( isset( $input['wp_comment_status_'.$i] ) )
                $new_input['wp_comment_status_'.$i] = $input['wp_comment_status_'.$i];
        }

        return $new_input;
    }

    // Make sure an array is used, cast the values if needed, and serialize for storage.
    private static function sanitize_multiselect($input,$cast = "Int")
    {
        $output = (Array)$input;
        foreach ($output as $k=>$v) {
            switch ($cast) {
                case "Int": $output[$k] = (Int)$v;
                case "String": $output[$k] = (String)$v;
                default: $output[$k] = $v;
            }
        }
        $output = serialize($output);
        return $output;
    }

    /** 
     * Print the Section header text
     */
    public function general_section_info()
    {
        print 'Choose how many different feeds of jobs you would like, then choose options for each feed';
    }

    /** 
     * Print the Section header text
     */
    public function feed_section_info()
    {
        print 'Choose what jobs you want and where you want them to go.';
    }

    public function number_of_feeds_callback()
    {
        printf(
            '<input type="text" id="number_of_feeds" name="caleb-connect-job_options[number_of_feeds]" value="%s" />',
            isset( $this->options['number_of_feeds'] ) ? esc_attr( $this->options['number_of_feeds']) : ''
        );
    }

    public function country_callback($args)
    {
        $i = $args['i'];
        $countries = (Array)unserialize($this->options['country_'.$i]);
            echo "<select id='country_$i' name='caleb-connect-job_options[country_$i][]' multiple='multiple'>";
            echo "<option value=''".(count($countries)<1 || $countries[0]=='' ? " selected":"").">Any</option>";
            foreach ($args['caleb_tags']['countries'] as $key=>$value) {
			   echo "<option value='$key'"; if (in_array($key,$countries)) echo " selected"; echo ">$value</option>>"; 
            }
          echo "</select>";
    }

    public function region_callback($args)
    {
        $i = $args['i'];
        $regions = (Array)unserialize($this->options['region_'.$i]);
            echo "<select id='region_$i' name='caleb-connect-job_options[region_$i][]' multiple='multiple'>";
            echo "<option value=''".(count($regions)<1 || $regions[0]=='' ? " selected":"").">Any</option>";
            foreach ($args['caleb_tags']['regions-religions'] as $key=>$value) {
			   echo "<option value='$key'"; if (in_array($key,$regions)) echo " selected"; echo ">$value</option>>"; 
            }
          echo "</select>";
    }

    public function category_callback($args)
    {
        $i = $args['i'];
        $categories = (Array)unserialize($this->options['category_'.$i]);
        echo "<select id='category_$i' name='caleb-connect-job_options[category_$i][]' multiple='multiple'>";
        echo "<option value=''".(count($categories)<1 || $categories[0]=='' ? " selected":"").">Any</option>";
        foreach ($args['caleb_tags']['categories'] as $key=>$value) {
           echo "<option value='$key'"; if (in_array($key,$categories)) echo " selected"; echo ">$value</option>>"; 
        }
        echo "</select>";
    }

    public function wp_category_callback($args)
    {
        $i = $args['i'];
        $category = $this->options['wp_category_'.$i];
        $wp_categories = get_terms('job_group',array('hide_empty'=>0));
        if (!count($wp_categories)) {
            wp_insert_term('Uncategorised','job_group');
            $wp_categories = get_terms('job_group',array('hide_empty'=>0));
        }
        echo "<select id='wp_category_$i' name='caleb-connect-job_options[wp_category_$i]'>";
        foreach ($wp_categories as $wp_category) {
           echo "<option value='$wp_category->term_id'"; if ($wp_category->term_id == $category) echo " selected"; echo ">$wp_category->name</option>>"; 
        }
        echo "</select>";
    }

    public function wp_tags_callback($args)
    {
        $i = $args['i'];
        echo "<input id='wp_category_$i' name='caleb-connect-job_options[wp_tags_$i]' value=\"".$this->options["wp_tags_$i"]."\">";
    }

    public function wp_status_callback($args)
    {
        $i = $args['i'];
        echo "<select id='wp_status_$i' name='caleb-connect-job_options[wp_status_$i]'>";
        foreach ($args['wp_statuses'] as $key=>$value) {
		   echo "<option value='$key'". ($key == $this->options['wp_status_'.$i] ? " selected":""). ">$value</option>>"; 
        }
        echo "</select>";
    }

    public function wp_comment_status_callback($args)
    {
        $i = $args['i'];
        echo "<select id='wp_comment_status_$i' name='caleb-connect-job_options[wp_comment_status_$i]'>";
        foreach ($args['wp_comment_statuses'] as $key=>$value) {
		   echo "<option value='$key'". ($key == $this->options['wp_comment_status_'.$i] ? " selected":""). ">$value</option>>"; 
        }
        echo "</select>";
    }

    /**
    * Parses the URL field to determine if the cache was cleared and prints to screen if so.
    */
    public function parse_message()
    {
        if ( ! isset ( $_GET['cache'] ) && ! isset ( $_GET['jobs'] ) && !get_transient('caleb-connect-job-error') 
            && ! isset ($_GET['trash'])
            && ! isset ($_GET['check'])
            )
            return;

        if ( isset ($_GET['imported'])) $imported = (int)$_GET['imported'];
        if ( isset ($_GET['updated'])) $updated = (int)$_GET['updated'];
        if ( isset ($_GET['kept'])) $kept = (int)$_GET['kept'];
        if ( isset ($_GET['trashed'])) $trashed = (int)$_GET['trashed'];

        if ( 'activated' === $_GET['cron'] )
            $this->text = 'Schedule activated';
        if ( 'deactivated' === $_GET['cron'] )
            $this->text = 'Schedule deactivated';
        else if ( 'done' === $_GET['check'] ) 
            $this->text = 'Check done. '.$trashed.' job(s) trashed';
        else if ( 'failed' === $_GET['check'] ) {
            $this->errors[] = 'Something went wrong when checking jobs are still on Caleb';
            $error = get_transient('caleb-connect-job-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-job-error');
        } else if ( 'got' === $_GET['jobs'] ) {
            $this->text = $imported.' new job(s) successfully converted to a post. '.$updated.' job(s) updated';
            if ($trashed) $this->text .= '. '.$trashed.' job(s) trashed';
        } else if ( 'failed' === $_GET['jobs'] ) {
            $this->errors[] = 'Something went wrong when importing jobs as posts';
            $error = get_transient('caleb-connect-job-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-job-error');
        } else if ( 'done' === $_GET['trash'] )
            $this->text = $trashed.' job(s) trashed';
        else if ( 'failed' === $_GET['trash'] ) {
            $this->errors[] = 'Something went wrong when trashing jobs';
            $error = get_transient('caleb-connect-job-error');
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-job-error');
        } else if ($error = get_transient('caleb-connect-job-error')) {
            $this->errors[] = 'Something went wrong automatically importing jobs as posts';
            $this->errors[] = date('d/m/y H:i',$error[0])." - $error[1]: $error[2]";
            delete_transient('caleb-connect-job-error');
        }
        else return;

        if (isset($this->text)) {
            echo '<div class="updated"><p>'
                . $this->text . '</p></div>';
        }
        if (isset($this->errors))
            if (count($this->errors)) {
                echo "<div class='error'>";
                foreach ($this->errors as $error)
                    echo "<p>$error</p>";
                echo "</div>";
            }
        unset($this->errors);
    }

}

if( is_admin() ) {
    $om_settings_page = new CalebJobsSettingsPage();
}

function get_jobs() {
    $jobGetter = new CalebJobGetter();
    $url = urldecode( admin_url( 'edit.php?post_type=caleb_job&page=caleb-connect-jobs') ) ;
    $result = $jobGetter->getNow();
    if ($result === false) 
        $url = add_query_arg( 'jobs', 'got', $url); 
    else {
        $url = add_query_arg( 'jobs', 'got', $url); 
        if (isset($jobGetter->number_trashed)) $url = add_query_arg( 'trashed', (int)$jobGetter->number_trashed, $url); 
        if (is_array($result)) {
            list($imported,$updated) = $result;
            $url = add_query_arg( 'imported',$imported,$url);
            $url = add_query_arg( 'updated',$updated,$url);
        }
    }
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_jobs_cron_activate() {
    if ( ! wp_next_scheduled('caleb_connect_jobs_cron') ) {
        wp_schedule_event( time(), 'daily', 'caleb_connect_jobs_cron');
    }
    $cron = 'activated';
    $url = add_query_arg( 'cron', $cron, urldecode( admin_url( 'edit.php?post_type=caleb_job&page=caleb-connect-jobs') ) );
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_jobs_cron_deactivate() {
    wp_clear_scheduled_hook('caleb_connect_jobs_cron');
    $cron = 'deactivated';
    $url = add_query_arg( 'cron', $cron, urldecode( admin_url( 'edit.php?post_type=caleb_job&page=caleb-connect-jobs') ) );
    wp_safe_redirect( $url );
    exit;
}

function caleb_connect_jobs_check_posts() {
    $getter = new CalebJobGetter();
    $url = urldecode( admin_url( 'edit.php?post_type=caleb_job&page=caleb-connect-jobs') ) ;
    $result = $getter->checkPosts();
    if ($result === false) 
        $url = add_query_arg( 'check', 'failed', $url); 
    else {
        $url = add_query_arg( 'check', 'done', $url); 
        $trashed = $result;
        $url = add_query_arg( 'trashed',(int)$trashed,$url);
    }
    wp_safe_redirect( $url );
    exit;
}

function trash_jobs() {
    $jobGetter = new CalebJobGetter();
    $trashed = $jobGetter->trashAll();
    if ($trashed===false) { $trash = 'failed';}
    else $trash = 'done';
    $url = add_query_arg( 'trash', $trash, urldecode( admin_url( 'edit.php?post_type=caleb_job&page=caleb-connect-jobs') ) );
    $url = add_query_arg( 'trashed',(int)$trashed,$url);
    wp_safe_redirect( $url );
    exit;
}
?>
